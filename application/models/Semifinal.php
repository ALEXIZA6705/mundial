<?php
 class  Semifinal extends CI_Model
 {
  public function __construct()
  {
   parent::__construct();
}
public function insertar($datos){
  return $this->db->insert("semifinal",$datos);
}
// consulta todos los estudiantes d ela base de datos
public function obtenerTodos(){
  $semifinales=$this->db->get("semifinal");
  if ($semifinales ->num_rows()>0) {
    return $semifinales;
  }else{
    return false; //cuando no existen los datos
  }

}
public function eliminarPorId($id_sem_ja){
        $this->db->where("id_sem_ja",$id_sem_ja);
        return $this->db->delete("semifinal");
      }
// consultando al estudiante por id
public function obtenerporId($id){
      $this->db->where("id_sem_ja",$id);
      $semifinal=$this->db->get("semifinal");
      if($semifinal->num_rows()>0){
        return $semifinal->row(); // solo biene uno
      }else{
        return false;
      }
   }
   // proceso de actualizacion del estudiante
   public function actualizar($id,$datos){
     $this->db->where ("id_sem_ja",$id);
     $this->db->update ("semifinal",$datos);
   }
}
