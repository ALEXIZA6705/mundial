<?php
 class  Grupoe extends CI_Model
 {
  public function __construct()
  {
   parent::__construct();
}
public function insertar($datos){
  return $this->db->insert("grupoe",$datos);
}
// consulta todos los estudiantes d ela base de datos
public function obtenerTodos(){
  $grupoes=$this->db->get("grupoe");
  if ($grupoes ->num_rows()>0) {
    return $grupoes;
  }else{
    return false; //cuando no existen los datos
  }

}
public function eliminarPorId($id_ge_ja){
        $this->db->where("id_ge_ja",$id_ge_ja);
        return $this->db->delete("grupoe");
      }
// consultando al estudiante por id
public function obtenerporId($id){
      $this->db->where("id_ge_ja",$id);
      $grupoe=$this->db->get("grupoe");
      if($grupoe->num_rows()>0){
        return $grupoe->row(); // solo biene uno
      }else{
        return false;
      }
   }
   // proceso de actualizacion del estudiante
   public function actualizar($id,$datos){
     $this->db->where ("id_ge_ja",$id);
     $this->db->update ("grupoe",$datos);
   }
}
