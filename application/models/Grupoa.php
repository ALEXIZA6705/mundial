<?php
 class  Grupoa extends CI_Model
 {
  public function __construct()
  {
   parent::__construct();
}
public function insertar($datos){
  return $this->db->insert("grupoa",$datos);
}
// consulta todos los estudiantes d ela base de datos
public function obtenerTodos(){
  $grupoas=$this->db->get("grupoa");
  if ($grupoas ->num_rows()>0) {
    return $grupoas;
  }else{
    return false; //cuando no existen los datos
  }

}
public function eliminarPorId($id_ga_ja){
        $this->db->where("id_ga_ja",$id_ga_ja);
        return $this->db->delete("grupoa");
      }
// consultando al estudiante por id
public function obtenerporId($id){
      $this->db->where("id_ga_ja",$id);
      $grupoa=$this->db->get("grupoa");
      if($grupoa->num_rows()>0){
        return $grupoa->row(); // solo biene uno
      }else{
        return false;
      }
   }
   // proceso de actualizacion del estudiante
   public function actualizar($id,$datos){
     $this->db->where ("id_ga_ja",$id);
     $this->db->update ("grupoa",$datos);
   }
}
