<?php
 class  Grupob extends CI_Model
 {
  public function __construct()
  {
   parent::__construct();
}
public function insertar($datos){
  return $this->db->insert("grupob",$datos);
}
// consulta todos los estudiantes d ela base de datos
public function obtenerTodos(){
  $grupobs=$this->db->get("grupob");
  if ($grupobs ->num_rows()>0) {
    return $grupobs;
  }else{
    return false; //cuando no existen los datos
  }

}
public function eliminarPorId($id_gb_ja){
        $this->db->where("id_gb_ja",$id_gb_ja);
        return $this->db->delete("grupob");
      }
// consultando al estudiante por id
public function obtenerporId($id){
      $this->db->where("id_gb_ja",$id);
      $grupob=$this->db->get("grupob");
      if($grupob->num_rows()>0){
        return $grupob->row(); // solo biene uno
      }else{
        return false;
      }
   }
   // proceso de actualizacion del estudiante
   public function actualizar($id,$datos){
     $this->db->where ("id_gb_ja",$id);
     $this->db->update ("grupob",$datos);
   }
}
