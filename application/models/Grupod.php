<?php
 class  Grupod extends CI_Model
 {
  public function __construct()
  {
   parent::__construct();
}
public function insertar($datos){
  return $this->db->insert("grupod",$datos);
}
// consulta todos los estudiantes d ela base de datos
public function obtenerTodos(){
  $grupods=$this->db->get("grupod");
  if ($grupods ->num_rows()>0) {
    return $grupods;
  }else{
    return false; //cuando no existen los datos
  }

}
public function eliminarPorId($id_gd_ja){
        $this->db->where("id_gd_ja",$id_gd_ja);
        return $this->db->delete("grupod");
      }
// consultando al estudiante por id
public function obtenerporId($id){
      $this->db->where("id_gd_ja",$id);
      $grupod=$this->db->get("grupod");
      if($grupod->num_rows()>0){
        return $grupod->row(); // solo biene uno
      }else{
        return false;
      }
   }
   // proceso de actualizacion del estudiante
   public function actualizar($id,$datos){
     $this->db->where ("id_gd_ja",$id);
     $this->db->update ("grupod",$datos);
   }
}
