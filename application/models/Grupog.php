<?php
 class  Grupog extends CI_Model
 {
  public function __construct()
  {
   parent::__construct();
}
public function insertar($datos){
  return $this->db->insert("grupog",$datos);
}
// consulta todos los estudiantes d ela base de datos
public function obtenerTodos(){
  $grupogs=$this->db->get("grupog");
  if ($grupogs ->num_rows()>0) {
    return $grupogs;
  }else{
    return false; //cuando no existen los datos
  }

}
public function eliminarPorId($id_gg_ja){
        $this->db->where("id_gg_ja",$id_gg_ja);
        return $this->db->delete("grupog");
      }
// consultando al estudiante por id
public function obtenerporId($id){
      $this->db->where("id_gg_ja",$id);
      $grupog=$this->db->get("grupog");
      if($grupog->num_rows()>0){
        return $grupog->row(); // solo biene uno
      }else{
        return false;
      }
   }
   // proceso de actualizacion del estudiante
   public function actualizar($id,$datos){
     $this->db->where ("id_gg_ja",$id);
     $this->db->update ("grupog",$datos);
   }
}
