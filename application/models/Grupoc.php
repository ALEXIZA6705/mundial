<?php
 class  Grupoc extends CI_Model
 {
  public function __construct()
  {
   parent::__construct();
}
public function insertar($datos){
  return $this->db->insert("grupoc",$datos);
}
// consulta todos los estudiantes d ela base de datos
public function obtenerTodos(){
  $grupocs=$this->db->get("grupoc");
  if ($grupocs ->num_rows()>0) {
    return $grupocs;
  }else{
    return false; //cuando no existen los datos
  }

}
public function eliminarPorId($id_gc_ja){
        $this->db->where("id_gc_ja",$id_gc_ja);
        return $this->db->delete("grupoc");
      }
// consultando al estudiante por id
public function obtenerporId($id){
      $this->db->where("id_gc_ja",$id);
      $grupoc=$this->db->get("grupoc");
      if($grupoc->num_rows()>0){
        return $grupoc->row(); // solo biene uno
      }else{
        return false;
      }
   }
   // proceso de actualizacion del estudiante
   public function actualizar($id,$datos){
     $this->db->where ("id_gc_ja",$id);
     $this->db->update ("grupoc",$datos);
   }
}
