<?php
 class Equipo extends CI_Model
 {
  public function __construct()
  {
   parent::__construct();
}
public function insertar($datos){
  return $this->db->insert("equipo",$datos);
}
// consulta todos los estudiantes d ela base de datos
public function obtenerTodos(){
  $equipos=$this->db->get("equipo");
  if ($equipos ->num_rows()>0) {
    return $equipos;
  }else{
    return false; //cuando no existen los datos
  }

}
public function eliminarPorId($id_ja){
        $this->db->where("id_ja",$id_ja);
        return $this->db->delete("equipo");
      }
// consultando al estudiante por id
public function obtenerporId($id){
      $this->db->where("id_ja",$id);
      $equipo=$this->db->get("equipo");
      if($equipo->num_rows()>0){
        return $equipo->row(); // solo biene uno
      }else{
        return false;
      }
   }
   // proceso de actualizacion del estudiante
   public function actualizar($id,$datos){
     $this->db->where ("id_ja",$id);
     $this->db->update ("equipo",$datos);
   }
}
