<?php
 class  Grupof extends CI_Model
 {
  public function __construct()
  {
   parent::__construct();
}
public function insertar($datos){
  return $this->db->insert("grupof",$datos);
}
// consulta todos los estudiantes d ela base de datos
public function obtenerTodos(){
  $grupofs=$this->db->get("grupof");
  if ($grupofs ->num_rows()>0) {
    return $grupofs;
  }else{
    return false; //cuando no existen los datos
  }

}
public function eliminarPorId($id_gf_ja){
        $this->db->where("id_gf_ja",$id_gf_ja);
        return $this->db->delete("grupof");
      }
// consultando al estudiante por id
public function obtenerporId($id){
      $this->db->where("id_gf_ja",$id);
      $grupof=$this->db->get("grupof");
      if($grupof->num_rows()>0){
        return $grupof->row(); // solo biene uno
      }else{
        return false;
      }
   }
   // proceso de actualizacion del estudiante
   public function actualizar($id,$datos){
     $this->db->where ("id_gf_ja",$id);
     $this->db->update ("grupof",$datos);
   }
}
