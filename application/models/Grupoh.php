<?php
 class  Grupoh extends CI_Model
 {
  public function __construct()
  {
   parent::__construct();
}
public function insertar($datos){
  return $this->db->insert("grupoh",$datos);
}
// consulta todos los estudiantes d ela base de datos
public function obtenerTodos(){
  $grupogs=$this->db->get("grupoh");
  if ($grupogs ->num_rows()>0) {
    return $grupogs;
  }else{
    return false; //cuando no existen los datos
  }

}
public function eliminarPorId($id_gh_ja){
        $this->db->where("id_gh_ja",$id_gh_ja);
        return $this->db->delete("grupoh");
      }
// consultando al estudiante por id
public function obtenerporId($id){
      $this->db->where("id_gh_ja",$id);
      $grupoh=$this->db->get("grupoh");
      if($grupoh->num_rows()>0){
        return $grupoh->row(); // solo biene uno
      }else{
        return false;
      }
   }
   // proceso de actualizacion del estudiante
   public function actualizar($id,$datos){
     $this->db->where ("id_gh_ja",$id);
     $this->db->update ("grupoh",$datos);
   }
}
