D<legend class="text-center">
  <i class="glyphicon glyphicon-plus"></i>
  AGREGAR NUEVO EQUIPO D
</legend>
<form id="frm_nuevo_grupod"class=""
enctype="multipart/form-data"
action="<?php echo site_url('grupods/guardarGrupod'); ?>" method="post">

<br>
<div class="row">
  <div class="col-md-4 text-right">
    <label for="">NUMERO GRUPO D:</label>
  </div>
  <div class="col-md-7">
    <input type="number" id="numero_gd_ja"name="numero_gd_ja" value=""class="form-control"placeholder="Ingrese el numero de grupo D" required>
  </div>
</div>
<br>
<div class="row">
  <div class="col-md-4 text-right">
    <label for="">Fotografia :</label>
  </div>
  <div class="col-md-7">
    <input type="file" id="foto_gd_ja" name="foto_gd_ja" value="required" accept="image/*"class="form-control"placeholder=" seleccione una foto "  required>
  </div>
</div>

<br>
<div class="row">
  <div class="col-md-4 text-right">
    <label for="">Pais:</label>
  </div>
  <div class="col-md-7">
    <input type="text" id="pais_gd_ja" name="pais_gd_ja" value=""class="form-control"placeholder="Ingrese el nombre del pais  "  required >
  </div>
</div>
<br>
<div class="row">
  <div class="col-md-4 text-right">
    <label for="">GOLES AFAVOR :</label>
  </div>
  <div class="col-md-7">
    <input type="text" id="favor_gd_ja" name="favor_gd_ja" value=""class="form-control"placeholder="Ingrese los goles a favor " required>
  </div>
</div>
<br>
<div class="row">
  <div class="col-md-4 text-right">
    <label for="">GOLES EN CONTRA  :</label>
  </div>
  <div class="col-md-7">
    <input type="text" id="contra_gd_ja" name="contra_gd_ja" value=""class="form-control"placeholder="Ingrese los goles en contra  " required>
  </div>
</div>
<br>
<div class="row">
  <div class="col-md-4 text-right">
    <label for="">GOLES de diferecncia  :</label>
  </div>
  <div class="col-md-7">
    <input type="text" id="diferencia_gd_ja" name="diferencia_gd_ja" value=""class="form-control"placeholder="Ingrese los goles a favor " required>
  </div>
</div>
<br>
<div class="row">
  <div class="col-md-4 text-right">
    <label for="">PUNTOS  :</label>
  </div>
  <div class="col-md-7">
    <input type="number" id="puntos_gd_ja" name="puntos_gd_ja" value=""class="form-control"placeholder="Ingrese los puntos " required>
  </div>
</div>
<br>

<br>
<br>
<div class="row">
  <div class="col-md-4">
  </div>
  <div class="col-md-7">
    <button type="submit" name="button" class="btn btn-primary">
<i class="glyphicon glyphicon-ok"></i>
Guardar
    </button>
    <a href="<?php echo site_url('grupods/index'); ?>" class="btn btn-danger">
<i class="glyphicon glyphicon-remove"></i>
Cancelar
    </a>
  </div>

</div>

</form>
<script type="text/javascript">
     $("#frm_nuevo_grupod").validate({
        rules:{
            numero_gd_ja:{
              required:true,
              minlength:10,
              maxlength:10,
              digits:true
            },
            foto_gd_ja:{
              required:true,
              minlength:10,
              maxlength:10,
              digits:true
            },
            pais_gd_ja:{
              required:true,
              minlength:10,
              maxlength:10,
              digits:true
            },
            favor_gd_ja:{
              required:true,
              minlength:3
            },
            contra_gd_ja:{
              required:true,
              minlength:3
            },
            diferencia_gd_ja:{
              required:true,
              minlength:3
            },
            puntos_gd_ja:{
              required:true,
              minlength:3
            }


        },
        messages:{
            numero_gd_ja:{
              required:"Por el nombre del continente",
              minlength:"continente incorrecto"
            },
            foto_gd_ja:{
              required:"Por el nombre del continente",
              minlength:"continente incorrecto"
            },


            pais_gd_ja:{
              required:"Por favor ingrese el nombre del pais",
              minlength:"Nombre incorrecto"
            },
            favor_ga_ja:{
              required:"Por favor ingrese el ranking global",
              minlength:"numero incorrecto",
              maxlength:"numero incorrecto",
              digits:"Este campo solo acepta números"
            },
            contra_gd_ja:{
              required:"Por favor ingrese el ranking global",
              minlength:"numero incorrecto",
              maxlength:"numero incorrecto",
              digits:"Este campo solo acepta números"
            },
            diferencia_gd_ja:{
              required:"Por favor ingrese el ranking global",
              minlength:"numero incorrecto",
              maxlength:"numero incorrecto",
              digits:"Este campo solo acepta números"
            },
            puntos_gd_ja:{
              required:"Por favor ingrese el ranking global",
              minlength:"numero incorrecto",
              maxlength:"numero incorrecto",
              digits:"Este campo solo acepta números"
            }


        },
     });
</script>
