<legend class="text-center">
  <!-- gestion de estudiantes -->
  <i class="glyphicon glyphicon-user" style="background-color: #e3f2fd;" ></i>
<b>  LISTADO DE GRUPO  H </b><br>
<br>

</legend>
<center>
  <a href="<?php echo site_url('grupohs/nuevo'); ?>" class="btn btn-success">
    <i class="glyphicon glyphicon-plus"></i>
    Agregar Nuevo Equipo
  </a>
</center>
<hr>
<?php if ($listadoGrupohs): ?>
  <table id = "tbl-grupohs"class="table table-striped table-bordered table-hover">
    <thead>
      <tr>
        <th class="text-center"> ID</th>
        <th class="text-center"> NUMERO</th>
        <th class="text-center"> PAIS </th>
        <th class="text-center">FAVOR</th>
        <th class="text-center">CONTRA</th>
        <th class="text-center">DIFERENCIA</th>
        <th class="text-center">PUNTOS</th>
        <th class="text-center"> FOTO</th>
        <th class="text-center"> ACCIONES</th>

      </tr>

    </thead>

    <tbody>
      <!-- $listadoClientes->result() as $clienteTemporal -->
        <?php foreach ($listadoGrupohs->result() as $grupohTemporal): ?>
          <tr>
            <td class="text-center"><?php echo $grupohTemporal->id_gh_ja; ?></td>
            <td class="text-center"><?php echo $grupohTemporal->numero_gh_ja;?></td>
            <td class="text-center"><?php echo $grupohTemporal->pais_gh_ja;?></td>
            <td class="text-center"><?php echo $grupohTemporal->favor_gh_ja; ?></td>
            <td class="text-center"><?php echo $grupohTemporal->contra_gh_ja; ?></td>
            <td class="text-center"><?php echo $grupohTemporal->diferencia_gh_ja; ?></td>
            <td class="text-center"><?php echo $grupohTemporal->puntos_gh_ja; ?></td>

            <td class="text-center">
              <?php if ($grupohTemporal->foto_gh_ja!=""): ?>
                      <a href="<?php echo base_url('uploads/grupohs').'/'.$grupohTemporal->foto_gh_ja; ?>"
                        target="_blank">
                        <img src="<?php echo base_url('uploads/grupohs').'/'.$grupohTemporal->foto_gh_ja; ?>"
                        width="50px" height="50px"
                        alt="">
                      </a>
                    <?php else: ?>
                      N/A
                    <?php endif; ?>
              </td>
              <!-- ruta <?php echo base_url('uploads/grupohs/').'/'.$grupohTemporal->foto_gh_ja ?> -->
            <td class="text-center">
              <a href="<?php echo site_url('grupohs/actualizar'); ?>/<?php echo $grupohTemporal->id_gh_ja ?>" class="btn btn-warning">
                <i class="glyphicon glyphicon-edit"></i>
                Editar
              </a>
              <a href="<?php echo site_url('grupohs/borrar'); ?>/<?php echo $grupohTemporal->id_gh_ja?>" class="btn btn-danger" onclick="return confirm('esta seguro de eliminar?');">
                <i class="glyphicon glyphicon-trash"></i>
                Eliminar
              </a>
            </td>
          </tr>
        <?php endforeach; ?>

    </tbody>

  </table>
<?php else: ?>
  <h3><b>No existen ningun grupo </b></h3>
<?php endif; ?>
<script type="text/javascript">
  $("#tbl-grupohs").DataTable();
</script>

<br>
<br>
