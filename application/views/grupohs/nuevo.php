<legend class="text-center">
  <i class="glyphicon glyphicon-plus"></i>
  AGREGAR NUEVO EQUIPO  H
</legend>
<form id="frm_nuevo_grupoh"class=""
enctype="multipart/form-data"
action="<?php echo site_url('grupohs/guardarGrupoh'); ?>" method="post">

<br>
<div class="row">
  <div class="col-md-4 text-right">
    <label for="">NUMERO GRUPO H :</label>
  </div>
  <div class="col-md-7">
    <input type="number" id="numero_gh_ja"name="numero_gh_ja" value=""class="form-control"placeholder="Ingrese el numero de grupo  H " required>
  </div>
</div>
<br>
<div class="row">
  <div class="col-md-4 text-right">
    <label for="">Fotografia :</label>
  </div>
  <div class="col-md-7">
    <input type="file" id="foto_gh_ja" name="foto_gh_ja" value="required" accept="image/*"class="form-control"placeholder=" seleccione una foto "  required>
  </div>
</div>

<br>
<div class="row">
  <div class="col-md-4 text-right">
    <label for="">Pais:</label>
  </div>
  <div class="col-md-7">
    <input type="text" id="pais_gh_ja" name="pais_gh_ja" value=""class="form-control"placeholder="Ingrese el nombre del pais  "  required >
  </div>
</div>
<br>
<div class="row">
  <div class="col-md-4 text-right">
    <label for="">GOLES AFAVOR :</label>
  </div>
  <div class="col-md-7">
    <input type="text" id="favor_gh_ja" name="favor_gh_ja" value=""class="form-control"placeholder="Ingrese los goles a favor " required>
  </div>
</div>
<br>
<div class="row">
  <div class="col-md-4 text-right">
    <label for="">GOLES EN CONTRA  :</label>
  </div>
  <div class="col-md-7">
    <input type="text" id="contra_gh_ja" name="contra_gh_ja" value=""class="form-control"placeholder="Ingrese los goles en contra  " required>
  </div>
</div>
<br>
<div class="row">
  <div class="col-md-4 text-right">
    <label for="">GOLES de diferecncia  :</label>
  </div>
  <div class="col-md-7">
    <input type="text" id="diferencia_gh_ja" name="diferencia_gh_ja" value=""class="form-control"placeholder="Ingrese los goles a favor " required>
  </div>
</div>
<br>
<div class="row">
  <div class="col-md-4 text-right">
    <label for="">PUNTOS  :</label>
  </div>
  <div class="col-md-7">
    <input type="number" id="puntos_gh_ja" name="puntos_gh_ja" value=""class="form-control"placeholder="Ingrese los puntos " required>
  </div>
</div>
<br>

<br>
<br>
<div class="row">
  <div class="col-md-4">
  </div>
  <div class="col-md-7">
    <button type="submit" name="button" class="btn btn-primary">
<i class="glyphicon glyphicon-ok"></i>
Guardar
    </button>
    <a href="<?php echo site_url('grupohs/index'); ?>" class="btn btn-danger">
<i class="glyphicon glyphicon-remove"></i>
Cancelar
    </a>
  </div>

</div>

</form>
<script type="text/javascript">
     $("#frm_nuevo_grupoh").validate({
        rules:{
            numero_gh_ja:{
              required:true,
              minlength:10,
              maxlength:10,
              digits:true
            },
            foto_gh_ja:{
              required:true,
              minlength:10,
              maxlength:10,
              digits:true
            },
            pais_gh_ja:{
              required:true,
              minlength:10,
              maxlength:10,
              digits:true
            },
            favor_gh_ja:{
              required:true,
              minlength:3
            },
            contra_gh_ja:{
              required:true,
              minlength:3
            },
            diferencia_gh_ja:{
              required:true,
              minlength:3
            },
            puntos_gh_ja:{
              required:true,
              minlength:3
            }


        },
        messages:{
            numero_gh_ja:{
              required:"Por el nombre del continente",
              minlength:"continente incorrecto"
            },
            foto_gh_ja:{
              required:"Por el nombre del continente",
              minlength:"continente incorrecto"
            },


            pais_gh_ja:{
              required:"Por favor ingrese el nombre del pais",
              minlength:"Nombre incorrecto"
            },
            favor_gh_ja:{
              required:"Por favor ingrese el ranking global",
              minlength:"numero incorrecto",
              maxlength:"numero incorrecto",
              digits:"Este campo solo acepta números"
            },
            contra_gh_ja:{
              required:"Por favor ingrese el ranking global",
              minlength:"numero incorrecto",
              maxlength:"numero incorrecto",
              digits:"Este campo solo acepta números"
            },
            diferencia_gh_ja:{
              required:"Por favor ingrese el ranking global",
              minlength:"numero incorrecto",
              maxlength:"numero incorrecto",
              digits:"Este campo solo acepta números"
            },
            puntos_gh_ja:{
              required:"Por favor ingrese el ranking global",
              minlength:"numero incorrecto",
              maxlength:"numero incorrecto",
              digits:"Este campo solo acepta números"
            }


        },
     });
</script>
