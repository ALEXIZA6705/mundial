<legend class="text-center">
  <i class="glyphicon glyphicon-plus"></i>
  AGREGAR NUEVO
</legend>
<form id="frm_nuevo_final"class=""
enctype="multipart/form-data"
action="<?php echo site_url('finales/guardarFinal'); ?>" method="post">

<br>
<div class="row">
  <div class="col-md-4 text-right">
    <label for="">Fotografia :</label>
  </div>
  <div class="col-md-7">
    <input type="file" id="foto_fin_ja" name="foto_fin_ja" value="required" accept="image/*"class="form-control"placeholder=" seleccione una foto "  required>
  </div>
</div>

<br>
<div class="row">
  <div class="col-md-4 text-right">
    <label for="">Pais:</label>
  </div>
  <div class="col-md-7">
    <input type="text" id="pais_fin_ja" name="pais_fin_ja" value=""class="form-control"placeholder="Ingrese el nombre del pais  "  required >
  </div>
</div>
<br>
<div class="row">
  <div class="col-md-4 text-right">
    <label for="">GRUPO :</label>
  </div>
  <div class="col-md-7">
    <input type="text" id="grupo_fin_ja" name="grupo_fin_ja" value=""class="form-control"placeholder="Ingrese el grupo" required>
  </div>
</div>
<br>
<div class="row">
  <div class="col-md-4 text-right">
    <label for="">GOLES   :</label>
  </div>
  <div class="col-md-7">
    <input type="text" id="goles_fin_ja" name="goles_fin_ja" value=""class="form-control"placeholder="Ingrese los goles en contra  " required>
  </div>
</div>

<br>
<br>
<div class="row">
  <div class="col-md-4">
  </div>
  <div class="col-md-7">
    <button type="submit" name="button" class="btn btn-primary">
<i class="glyphicon glyphicon-ok"></i>
Guardar
    </button>
    <a href="<?php echo site_url('finales/index'); ?>" class="btn btn-danger">
<i class="glyphicon glyphicon-remove"></i>
Cancelar
    </a>
  </div>

</div>

</form>
<script type="text/javascript">
     $("#frm_nuevo_final").validate({
        rules:{
            foto_fin_ja:{
              required:true,
              minlength:10,
              maxlength:10,
              digits:true
            },
            pais_fin_ja:{
              required:true,
              minlength:10,
              maxlength:10,
              digits:true
            },
            grupo_fin_ja:{
              required:true,
              minlength:3
            },
            goles_fin_ja:{
              required:true,
          }
            },
          messages:{
              foto_fin_ja:{
                required:"Por el nombre del continente",
                minlength:"continente incorrecto"
              },

              pais_fin_ja:{
                required:"Por favor ingrese el nombre del pais",
                minlength:"Nombre incorrecto"
              },
            grupo_fin_ja:{
                required:"Por favor ingrese el ranking global",
                minlength:"numero incorrecto",
                maxlength:"numero incorrecto",
                digits:"Este campo solo acepta números"
              },
              goles_fin_ja:{
                  required:"Por favor ingrese el ranking global",
                  minlength:"numero incorrecto",
                  maxlength:"numero incorrecto",
                  digits:"Este campo solo acepta números"
                }


            },
         });
    </script>
