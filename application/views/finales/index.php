<legend class="text-center">
  <!-- gestion de estudiantes -->
  <i class="glyphicon glyphicon-user" style="background-color: #E4F4E9;" ></i>
<b>  LISTADO DE FINAL </b><br>
<br>

</legend>
<center>
  <a href="<?php echo site_url('finales/nuevo'); ?>" class="btn btn-success">
    <i class="glyphicon glyphicon-plus"></i>
    Agregar Nuevo
  </a>
</center>
<hr>
<?php if ($listadoFinales): ?>
  <table id = "tbl-finales"class="table table-striped table-bordered table-hover">
    <thead>
      <tr>
        <th class="text-center"> ID</th>
        <th class="text-center"> PAIS </th>
        <th class="text-center">GRUPO</th>
        <th class="text-center">GOLES</th>
        <th class="text-center"> FOTO</th>
        <th class="text-center"> ACCIONES</th>

      </tr>

    </thead>

    <tbody>
      <!-- $listadoClientes->result() as $clienteTemporal -->
        <?php foreach ($listadoFinales->result() as $finalTemporal): ?>
          <tr>
            <td class="text-center"><?php echo $finalTemporal->id_fin_ja; ?></td>
            <td class="text-center"><?php echo $finalTemporal->pais_fin_ja;?></td>
            <td class="text-center"><?php echo $finalTemporal->grupo_fin_ja; ?></td>
            <td class="text-center"><?php echo $finalTemporal->goles_fin_ja; ?></td>

            <td class="text-center">
              <?php if ($finalTemporal->foto_fin_ja!=""): ?>
                      <a href="<?php echo base_url('uploads/finales').'/'.$finalTemporal->foto_fin_ja; ?>"
                        target="_blank">
                        <img src="<?php echo base_url('uploads/finales').'/'.$finalTemporal->foto_fin_ja; ?>"
                        width="50px" height="50px"
                        alt="">
                      </a>
                    <?php else: ?>
                      N/A
                    <?php endif; ?>
              </td>
              <!-- ruta <?php echo base_url('uploads/finales/').'/'.$finalTemporal->foto_fin_ja ?> -->
            <td class="text-center">
              <a href="<?php echo site_url('finales/actualizar'); ?>/<?php echo $finalTemporal->id_fin_ja ?>" class="btn btn-warning">
                <i class="glyphicon glyphicon-edit"></i>
                Editar
              </a>
              <a href="<?php echo site_url('finales/borrar'); ?>/<?php echo $finalTemporal->id_fin_ja?>" class="btn btn-danger" onclick="return confirm('esta seguro de eliminar?');">
                <i class="glyphicon glyphicon-trash"></i>
                Eliminar
              </a>
            </td>
          </tr>
        <?php endforeach; ?>

    </tbody>

  </table>
<?php else: ?>
  <h3><b>No existen ningun grupo </b></h3>
<?php endif; ?>
<script type="text/javascript">
  $("#tbl-finales").DataTable();
</script>

<br>
<br>
