<div class="row">
  <div class="col-md-12 text-center well">
    <h3>ACTUALIZAR FINAL </h3>
    <center>
      <a href="<?php echo site_url('finales/index') ?>" class="btn btn-success">
      <i class="glyphicon glyphicon-repeat"></i>
      REGRESAR
      </a>
    </center>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <?php if ($finalesEditar): ?>
      <!-- <?php print_r($finalesEditar); ?> -->
      <!-- copiamos el formulario para haorrarnos tiempo -->
      <form  class="" action="<?php echo site_url('finales/procesarActualizacion') ?>" method="post">
        <center>
          <input type="text" name="id_fin_ja" value="<?php echo $finalesEditar->id_fin_ja; ?> "></input>
          <!-- //hidden sirve para ocultar  el formulario -->
        </center>


        <div class="row">
          <div class="col-md-4 text-right">
            <label for="">Pais</label>
          </div>
          <div class="col-md-7">
            <!-- el tipo de comentarios -->
            <input type="text" name="pais_fin_ja" value="<?php echo $finalesEditar->pais_fin_ja; ?>"
            class="form-control" placeholder="Ingrese el nombre del Pais" required>
          </div>
        </div>
        <br>
        <div class="row">
          <div class="col-md-4 text-right">
            <label for="">Grupo  </label>
          </div>
          <div class="col-md-7">
            <!-- el tipo de comentarios -->
            <input type="text" name="grupo_fin_ja" value="<?php echo $finalesEditar->grupo_fin_ja; ?>"
            class="form-control" placeholder="Ingrese el grupo " required>
          </div>
        </div>
        <br>
        <div class="row">
          <div class="col-md-4 text-right">
            <label for=""> Goles  </label>
          </div>
          <div class="col-md-7">
            <!-- el tipo de comentarios -->
            <input type="text" name="goles_fin_ja" value="<?php echo $finalesEditar->goles_fin_ja; ?>"
            class="form-control" placeholder="Ingrese goles  " required>
          </div>
        </div>
        <br>


      <div class="row">
        <div class="col-md-4">
        </div>
        <div class="col-md-7">
          <!-- cambianos el nombre de button a submit -->
          <button type="submit" name="button"
                class="btn btn-warning">
              <i class="glyphicon glyphicon-ok"></i>
              ACTUALIZAR
          </button>
          <a href="<?php echo site_url('finales/index') ?>" class="btn btn-danger">
            <i class="glyphicon glyphicon-remove"></i>
            CANCELAR
          </a>
        </div>
      </div>
      </form>

    <?php else: ?>
      <div class="alert alert-danger">
        <b>NO SE ENCONTRO NUEVOS INGRESOS  </b>
      </div>
    <?php endif; ?>
  </div>
</div>
