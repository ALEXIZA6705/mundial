<div class="row">
  <div class="col-md-12 text-center well">
    <h3>ACTUALIZAR EL GRUPO f</h3>
    <center>
      <a href="<?php echo site_url('grupofs/index') ?>" class="btn btn-success">
      <i class="glyphicon glyphicon-repeat"></i>
      REGRESAR
      </a>
    </center>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <?php if ($grupofsEditar): ?>
      <!-- <?php print_r($grupofsEditar); ?> -->
      <!-- copiamos el formulario para haorrarnos tiempo -->
      <form  class="" action="<?php echo site_url('grupofs/procesarActualizacion') ?>" method="post">
        <center>
          <input type="text" name="id_gf_ja" value="<?php echo $grupofsEditar->id_gf_ja; ?> "></input>
          <!-- //hidden sirve para ocultar  el formulario -->
        </center>
        <br>
        <div class="row">
          <div class="col-md-4 text-right">
            <label for="">numero </label>
          </div>
          <div class="col-md-7">
            <input type="numero" name="numero_gf_ja" value="<?php echo $grupofsEditar-> numero_gf_ja ?>"
            class="form-control" placeholder="Ingrese el numero  " required>
          </div>
        </div>
        <br>

        <div class="row">
          <div class="col-md-4 text-right">
            <label for="">Pais</label>
          </div>
          <div class="col-md-7">
            <!-- el tipo de comentarios -->
            <input type="text" name="pais_gf_ja" value="<?php echo $grupofsEditar->pais_gf_ja; ?>"
            class="form-control" placeholder="Ingrese el nombre del Pais" required>
          </div>
        </div>
        <br>
        <div class="row">
          <div class="col-md-4 text-right">
            <label for=""> Goles a favor </label>
          </div>
          <div class="col-md-7">
            <!-- el tipo de comentarios -->
            <input type="text" name="favor_gf_ja" value="<?php echo $grupofsEditar->favor_gf_ja; ?>"
            class="form-control" placeholder="Ingrese goles a favor " required>
          </div>
        </div>
        <br>
        <div class="row">
          <div class="col-md-4 text-right">
            <label for=""> Goles en contra </label>
          </div>
          <div class="col-md-7">
            <!-- el tipo de comentarios -->
            <input type="text" name="contra_gf_ja" value="<?php echo $grupofsEditar->contra_gf_ja; ?>"
            class="form-control" placeholder="Ingrese goles encontra " required>
          </div>
        </div>
        <br>
        <div class="row">
          <div class="col-md-4 text-right">
            <label for="">Goles de diferencia </label>
          </div>
          <div class="col-md-7">
            <!-- el tipo de comentarios -->
            <input type="text" name="diferencia_gf_ja" value="<?php echo $grupofsEditar->diferencia_gf_ja; ?>"
            class="form-control" placeholder="Ingrese la diferencia " required>
          </div>
        </div>
        <br>

        <div class="row">
          <div class="col-md-4 text-right">
            <label for=""> puntos  </label>
          </div>
          <div class="col-md-7">
            <!-- el tipo de comentarios -->
            <input type="text" name="puntos_gf_ja" value="<?php echo $grupofsEditar->puntos_gf_ja; ?>"
            class="form-control" placeholder="Ingrese los puntos " required>
          </div>
        </div>
        <br>

      <div class="row">
        <div class="col-md-4">
        </div>
        <div class="col-md-7">
          <!-- cambianos el nombre de button a submit -->
          <button type="submit" name="button"
                class="btn btn-warning">
              <i class="glyphicon glyphicon-ok"></i>
              ACTUALIZAR
          </button>
          <a href="<?php echo site_url('grupofs/index') ?>" class="btn btn-danger">
            <i class="glyphicon glyphicon-remove"></i>
            CANCELAR
          </a>
        </div>
      </div>
      </form>

    <?php else: ?>
      <div class="alert alert-danger">
        <b>NO SE ENCONTRO NUEVOS INGRESOS  </b>
      </div>
    <?php endif; ?>
  </div>
</div>
