<legend class="text-center">
  <!-- gestion de estudiantes -->
  <i class="glyphicon glyphicon-user" style="background-color: #e3f2fd;" ></i>
<b>  LISTADO DE GRUPO  F</b><br>
<br>

</legend>
<center>
  <a href="<?php echo site_url('grupofs/nuevo'); ?>" class="btn btn-success">
    <i class="glyphicon glyphicon-plus"></i>
    Agregar Nuevo Equipo
  </a>
</center>
<hr>
<?php if ($listadoGrupofs): ?>
  <table id = "tbl-grupofs"class="table table-striped table-bordered table-hover">
    <thead>
      <tr>
        <th class="text-center"> ID</th>
        <th class="text-center"> NUMERO</th>
        <th class="text-center"> PAIS </th>
        <th class="text-center">FAVOR</th>
        <th class="text-center">CONTRA</th>
        <th class="text-center">DIFERENCIA</th>
        <th class="text-center">PUNTOS</th>
        <th class="text-center"> FOTO</th>
        <th class="text-center"> ACCIONES</th>

      </tr>

    </thead>

    <tbody>
      <!-- $listadoClientes->result() as $clienteTemporal -->
        <?php foreach ($listadoGrupofs->result() as $grupofTemporal): ?>
          <tr>
            <td class="text-center"><?php echo $grupofTemporal->id_gf_ja; ?></td>
            <td class="text-center"><?php echo $grupofTemporal->numero_gf_ja;?></td>
            <td class="text-center"><?php echo $grupofTemporal->pais_gf_ja;?></td>
            <td class="text-center"><?php echo $grupofTemporal->favor_gf_ja; ?></td>
            <td class="text-center"><?php echo $grupofTemporal->contra_gf_ja; ?></td>
            <td class="text-center"><?php echo $grupofTemporal->diferencia_gf_ja; ?></td>
            <td class="text-center"><?php echo $grupofTemporal->puntos_gf_ja; ?></td>

            <td class="text-center">
              <?php if ($grupofTemporal->foto_gf_ja!=""): ?>
                      <a href="<?php echo base_url('uploads/grupofs').'/'.$grupofTemporal->foto_gf_ja; ?>"
                        target="_blank">
                        <img src="<?php echo base_url('uploads/grupofs').'/'.$grupofTemporal->foto_gf_ja; ?>"
                        width="50px" height="50px"
                        alt="">
                      </a>
                    <?php else: ?>
                      N/A
                    <?php endif; ?>
              </td>
              <!-- ruta <?php echo base_url('uploads/grupofs/').'/'.$grupofTemporal->foto_gf_ja ?> -->
            <td class="text-center">
              <a href="<?php echo site_url('grupofs/actualizar'); ?>/<?php echo $grupofTemporal->id_gf_ja ?>" class="btn btn-warning">
                <i class="glyphicon glyphicon-edit"></i>
                Editar
              </a>
              <a href="<?php echo site_url('grupofs/borrar'); ?>/<?php echo $grupofTemporal->id_gf_ja?>" class="btn btn-danger" onclick="return confirm('esta seguro de eliminar?');">
                <i class="glyphicon glyphicon-trash"></i>
                Eliminar
              </a>
            </td>
          </tr>
        <?php endforeach; ?>

    </tbody>

  </table>
<?php else: ?>
  <h3><b>No existen ningun grupo </b></h3>
<?php endif; ?>
<script type="text/javascript">
  $("#tbl-grupofs").DataTable();
</script>

<br>
<br>
