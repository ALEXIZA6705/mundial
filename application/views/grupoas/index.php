<legend class="text-center">
  <!-- gestion de estudiantes -->
  <i class="glyphicon glyphicon-user" style="background-color: #e3f2fd;" ></i>
<b>  LISTADO DE GRUPO A</b><br>
<br>

</legend>
<center>
  <a href="<?php echo site_url('grupoas/nuevo'); ?>" class="btn btn-success">
    <i class="glyphicon glyphicon-plus"></i>
    Agregar Nuevo Equipo
  </a>
</center>
<hr>
<?php if ($listadoGrupoas): ?>
  <table id = "tbl-grupoas"class="table table-striped table-bordered table-hover">
    <thead>
      <tr>
        <th class="text-center"> ID</th>
        <th class="text-center"> NUMERO</th>
        <th class="text-center"> PAIS </th>
        <th class="text-center">FAVOR</th>
        <th class="text-center">CONTRA</th>
        <th class="text-center">DIFERENCIA</th>
        <th class="text-center">PUNTOS</th>
        <th class="text-center"> FOTO</th>
        <th class="text-center"> ACCIONES</th>

      </tr>

    </thead>

    <tbody>
      <!-- $listadoClientes->result() as $clienteTemporal -->
        <?php foreach ($listadoGrupoas->result() as $grupoaTemporal): ?>
          <tr>
            <td class="text-center"><?php echo $grupoaTemporal->id_ga_ja; ?></td>
            <td class="text-center"><?php echo $grupoaTemporal->numero_ga_ja;?></td>
            <td class="text-center"><?php echo $grupoaTemporal->pais_ga_ja;?></td>
            <td class="text-center"><?php echo $grupoaTemporal->favor_ga_ja; ?></td>
            <td class="text-center"><?php echo $grupoaTemporal->contra_ga_ja; ?></td>
            <td class="text-center"><?php echo $grupoaTemporal->diferencia_ga_ja; ?></td>
            <td class="text-center"><?php echo $grupoaTemporal->puntos_ga_ja; ?></td>

            <td>
                <?php if ($grupoaTemporal->foto_ga_ja!=""): ?>
                    <a href="<?php echo base_url('uploads/grupoas').'/'.$grupoarTemporal->foto_ga_ja; ?>"
                      target="_blank">
                      <img src="<?php echo base_url('uploads/grupoas').'/'.$grupoarTemporal->foto_ga_ja; ?>"
                      width="50px" height="50px"
                      alt="">
                    </a>
                  <?php else: ?>
                    N/A
                  <?php endif; ?>
                </td>
              <!-- ruta <?php echo base_url('uploads/grupoas/').'/'.$grupoaTemporal->foto_ga_ja ?> -->
            <td class="text-center">
              <a href="<?php echo site_url('grupoas/actualizar'); ?>/<?php echo $grupoaTemporal->id_ga_ja ?>" class="btn btn-warning">
                <i class="glyphicon glyphicon-edit"></i>
                Editar
              </a>
              <a href="<?php echo site_url('grupoas/borrar'); ?>/<?php echo $grupoaTemporal->id_ga_ja?>" class="btn btn-danger" onclick="return confirm('esta seguro de eliminar?');">
                <i class="glyphicon glyphicon-trash"></i>
                Eliminar
              </a>
            </td>
          </tr>
        <?php endforeach; ?>

    </tbody>

  </table>
<?php else: ?>
  <h3><b>No existen ningun grupo </b></h3>
<?php endif; ?>
<script type="text/javascript">
  $("#tbl-grupoas").DataTable();
</script>

<br>
<br>
