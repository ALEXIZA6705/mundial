<legend class="text-center">
  <i class="glyphicon glyphicon-plus"></i>
  AGREGAR NUEVO EQUIPO A
</legend>
<form id="frm_nuevo_grupoa"class=""
enctype="multipart/form-data"
action="<?php echo site_url('grupoas/guardarGrupoa'); ?>" method="post">

<br>
<div class="row">
  <div class="col-md-4 text-right">
    <label for="">NUMERO GRUPO A:</label>
  </div>
  <div class="col-md-7">
    <input type="number" id="numero_ga_ja"name="numero_ga_ja" value=""class="form-control"placeholder="Ingrese el numero de grupo A" required>
  </div>
</div>
<br>
  <div class="row">
      <div class="col-md-4 text-right">
        <label for="">FOTO</label>

      </div>
      <div class="col-md-7">
        <input type="file" id="foto_ga_ja" name="foto_ga_ja" value=""class="form-control" placeholder="Seleccione la fotografia" required accept="image/*">
      </div>
    </div>



    <br>
<div class="row">
  <div class="col-md-4 text-right">
    <label for="">Pais:</label>
  </div>
  <div class="col-md-7">
    <input type="text" id="pais_ga_ja" name="pais_ga_ja" value=""class="form-control"placeholder="Ingrese el nombre del pais  "  required >
  </div>
</div>
<br>
<div class="row">
  <div class="col-md-4 text-right">
    <label for="">GOLES AFAVOR :</label>
  </div>
  <div class="col-md-7">
    <input type="text" id="favor_ga_ja" name="favor_ga_ja" value=""class="form-control"placeholder="Ingrese los goles a favor " required>
  </div>
</div>
<br>
<div class="row">
  <div class="col-md-4 text-right">
    <label for="">GOLES EN CONTRA  :</label>
  </div>
  <div class="col-md-7">
    <input type="text" id="contra_ga_ja" name="contra_ga_ja" value=""class="form-control"placeholder="Ingrese los goles en contra  " required>
  </div>
</div>
<br>
<div class="row">
  <div class="col-md-4 text-right">
    <label for="">GOLES de diferecncia  :</label>
  </div>
  <div class="col-md-7">
    <input type="text" id="diferencia_ga_ja" name="diferencia_ga_ja" value=""class="form-control"placeholder="Ingrese los goles a favor " required>
  </div>
</div>
<br>
<div class="row">
  <div class="col-md-4 text-right">
    <label for="">PUNTOS  :</label>
  </div>
  <div class="col-md-7">
    <input type="number" id="puntos_ga_ja" name="puntos_ga_ja" value=""class="form-control"placeholder="Ingrese los puntos " required>
  </div>
</div>
<br>

<br>
<br>
<div class="row">
  <div class="col-md-4">
  </div>
  <div class="col-md-7">
    <button type="submit" name="button" class="btn btn-primary">
<i class="glyphicon glyphicon-ok"></i>
Guardar
    </button>
    <a href="<?php echo site_url('grupoas/index'); ?>" class="btn btn-danger">
<i class="glyphicon glyphicon-remove"></i>
Cancelar
    </a>
  </div>

</div>

</form>

<script type="text/javascript">
     $("#frm_nuevo_grupoa").validate({
        rules:{
            numero_ga_ja:{
              required:true,
              minlength:10,
              maxlength:10,
              digits:true
            },
            foto_ga_ja:{
              required:true,
              minlength:10,
              maxlength:10,
              digits:true
            },
            pais_ga_ja:{
              required:true,
              minlength:10,
              maxlength:10,
              digits:true
            },
            favor_ga_ja:{
              required:true,
              minlength:3
            },
            contra_ga_ja:{
              required:true,
              minlength:3
            },
            diferencia_ga_ja:{
              required:true,
              minlength:3
            },
            puntos_ga_ja:{
              required:true,
              minlength:3
            }


        },
        messages:{
            numero_ga_ja:{
              required:"Por el nombre del continente",
              minlength:"continente incorrecto"
            },
            foto_ga_ja:{
              required:"Por el nombre del continente",
              minlength:"continente incorrecto"
            },


            pais_ga_ja:{
              required:"Por favor ingrese el nombre del pais",
              minlength:"Nombre incorrecto"
            },
            favor_ga_ja:{
              required:"Por favor ingrese el ranking global",
              minlength:"numero incorrecto",
              maxlength:"numero incorrecto",
              digits:"Este campo solo acepta números"
            },
            contra_ga_ja:{
              required:"Por favor ingrese el ranking global",
              minlength:"numero incorrecto",
              maxlength:"numero incorrecto",
              digits:"Este campo solo acepta números"
            },
            diferencia_ga_ja:{
              required:"Por favor ingrese el ranking global",
              minlength:"numero incorrecto",
              maxlength:"numero incorrecto",
              digits:"Este campo solo acepta números"
            },
            puntos_ga_ja:{
              required:"Por favor ingrese el ranking global",
              minlength:"numero incorrecto",
              maxlength:"numero incorrecto",
              digits:"Este campo solo acepta números"
            }


        },
     });
</script>
