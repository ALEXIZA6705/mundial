<legend class="text-center">
  <!-- gestion de estudiantes -->
  <i class="glyphicon glyphicon-user" style="background-color: #e3f2fd;" ></i>
<b>  LISTADO DE GRUPO C</b><br>
<br>

</legend>
<center>
  <a href="<?php echo site_url('grupocs/nuevo'); ?>" class="btn btn-success">
    <i class="glyphicon glyphicon-plus"></i>
    Agregar Nuevo Equipo
  </a>
</center>
<hr>
<?php if ($listadoGrupocs): ?>
  <table id = "tbl-grupocs"class="table table-striped table-bordered table-hover">
    <thead>
      <tr>
        <th class="text-center"> ID</th>
        <th class="text-center"> NUMERO</th>
        <th class="text-center"> PAIS </th>
        <th class="text-center">FAVOR</th>
        <th class="text-center">CONTRA</th>
        <th class="text-center">DIFERENCIA</th>
        <th class="text-center">PUNTOS</th>
        <th class="text-center"> FOTO</th>
        <th class="text-center"> ACCIONES</th>

      </tr>

    </thead>

    <tbody>
      <!-- $listadoClientes->result() as $clienteTemporal -->
        <?php foreach ($listadoGrupocs->result() as $grupocTemporal): ?>
          <tr>
            <td class="text-center"><?php echo $grupocTemporal->id_gc_ja; ?></td>
            <td class="text-center"><?php echo $grupocTemporal->numero_gc_ja;?></td>
            <td class="text-center"><?php echo $grupocTemporal->pais_gc_ja;?></td>
            <td class="text-center"><?php echo $grupocTemporal->favor_gc_ja; ?></td>
            <td class="text-center"><?php echo $grupocTemporal->contra_gc_ja; ?></td>
            <td class="text-center"><?php echo $grupocTemporal->diferencia_gc_ja; ?></td>
            <td class="text-center"><?php echo $grupocTemporal->puntos_gc_ja; ?></td>

            <td class="text-center">
              <?php if ($grupocTemporal->foto_gc_ja!=""): ?>
                      <a href="<?php echo base_url('uploads/grupocs').'/'.$grupocTemporal->foto_gc_ja; ?>"
                        target="_blank">
                        <img src="<?php echo base_url('uploads/grupocs').'/'.$grupocTemporal->foto_gc_ja; ?>"
                        width="50px" height="50px"
                        alt="">
                      </a>
                    <?php else: ?>
                      N/A
                    <?php endif; ?>
              </td>
              <!-- ruta <?php echo base_url('uploads/grupocs/').'/'.$grupocTemporal->foto_gc_ja ?> -->
            <td class="text-center">
              <a href="<?php echo site_url('grupocs/actualizar'); ?>/<?php echo $grupocTemporal->id_gc_ja ?>" class="btn btn-warning">
                <i class="glyphicon glyphicon-edit"></i>
                Editar
              </a>
              <a href="<?php echo site_url('grupocs/borrar'); ?>/<?php echo $grupocTemporal->id_gc_ja?>" class="btn btn-danger" onclick="return confirm('esta seguro de eliminar?');">
                <i class="glyphicon glyphicon-trash"></i>
                Eliminar
              </a>
            </td>
          </tr>
        <?php endforeach; ?>

    </tbody>

  </table>
<?php else: ?>
  <h3><b>No existen ningun grupo </b></h3>
<?php endif; ?>
<script type="text/javascript">
  $("#tbl-grupocs").DataTable();
</script>

<br>
<br>
