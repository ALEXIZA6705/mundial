<legend class="text-center">
  <!-- gestion de estudiantes -->
  <i class="glyphicon glyphicon-user" style="background-color: #e3f2fd;" ></i>
<b>  LISTADO DE GRUPO B</b><br>
<br>

</legend>
<center>
  <a href="<?php echo site_url('grupobs/nuevo'); ?>" class="btn btn-success">
    <i class="glyphicon glyphicon-plus"></i>
    Agregar Nuevo Equipo
  </a>
</center>
<hr>
<?php if ($listadoGrupobs): ?>
  <table id = "tbl-grupobs"class="table table-striped table-bordered table-hover">
    <thead>
      <tr>
        <th class="text-center"> ID</th>
        <th class="text-center"> NUMERO</th>
        <th class="text-center"> PAIS </th>
        <th class="text-center">FAVOR</th>
        <th class="text-center">CONTRA</th>
        <th class="text-center">DIFERENCIA</th>
        <th class="text-center">PUNTOS</th>
        <th class="text-center"> FOTO</th>
        <th class="text-center"> ACCIONES</th>

      </tr>

    </thead>

    <tbody>
      <!-- $listadoClientes->result() as $clienteTemporal -->
        <?php foreach ($listadoGrupobs->result() as $grupobTemporal): ?>
          <tr>
            <td class="text-center"><?php echo $grupobTemporal->id_gb_ja; ?></td>
            <td class="text-center"><?php echo $grupobTemporal->numero_gb_ja;?></td>
            <td class="text-center"><?php echo $grupobTemporal->pais_gb_ja;?></td>
            <td class="text-center"><?php echo $grupobTemporal->favor_gb_ja; ?></td>
            <td class="text-center"><?php echo $grupobTemporal->contra_gb_ja; ?></td>
            <td class="text-center"><?php echo $grupobTemporal->diferencia_gb_ja; ?></td>
            <td class="text-center"><?php echo $grupobTemporal->puntos_gb_ja; ?></td>

            <td class="text-center">
              <?php if ($grupobTemporal->foto_gb_ja!=""): ?>
                      <a href="<?php echo base_url('uploads/grupobs').'/'.$grupobTemporal->foto_gb_ja; ?>"
                        target="_blank">
                        <img src="<?php echo base_url('uploads/grupobs').'/'.$grupobTemporal->foto_gb_ja; ?>"
                        width="50px" height="50px"
                        alt="">
                      </a>
                    <?php else: ?>
                      N/A
                    <?php endif; ?>
              </td>
              <!-- ruta <?php echo base_url('uploads/grupobs/').'/'.$grupobTemporal->foto_gb_ja ?> -->
            <td class="text-center">
              <a href="<?php echo site_url('grupobs/actualizar'); ?>/<?php echo $grupobTemporal->id_gb_ja ?>" class="btn btn-warning">
                <i class="glyphicon glyphicon-edit"></i>
                Editar
              </a>
              <a href="<?php echo site_url('grupobs/borrar'); ?>/<?php echo $grupobTemporal->id_gb_ja?>" class="btn btn-danger" onclick="return confirm('esta seguro de eliminar?');">
                <i class="glyphicon glyphicon-trash"></i>
                Eliminar
              </a>
            </td>
          </tr>
        <?php endforeach; ?>

    </tbody>

  </table>
<?php else: ?>
  <h3><b>No existen ningun grupo </b></h3>
<?php endif; ?>
<script type="text/javascript">
  $("#tbl-grupobs").DataTable();
</script>

<br>
<br>
