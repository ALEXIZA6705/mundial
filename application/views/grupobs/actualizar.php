<div class="row">
  <div class="col-md-12 text-center well">
    <h3>ACTUALIZAR EL GRUPO B </h3>
    <center>
      <a href="<?php echo site_url('grupobs/index') ?>" class="btn btn-success">
      <i class="glyphicon glyphicon-repeat"></i>
      REGRESAR
      </a>
    </center>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <?php if ($grupobsEditar): ?>
      <!-- <?php print_r($grupobsEditar); ?> -->
      <!-- copiamos el formulario para haorrarnos tiempo -->
      <form  class="" action="<?php echo site_url('grupobs/procesarActualizacion') ?>" method="post">
        <center>
          <input type="text" name="id_ga_ja" value="<?php echo $grupobsEditar->id_gb_ja; ?> "></input>
          <!-- //hidden sirve para ocultar  el formulario -->
        </center>
        <br>
        <div class="row">
          <div class="col-md-4 text-right">
            <label for="">numero </label>
          </div>
          <div class="col-md-7">
            <input type="numero" name="numero_gb_ja" value="<?php echo $grupobsEditar-> numero_gb_ja ?>"
            class="form-control" placeholder="Ingrese el numero  " required>
          </div>
        </div>
        <br>

        <div class="row">
          <div class="col-md-4 text-right">
            <label for="">Pais</label>
          </div>
          <div class="col-md-7">
            <!-- el tipo de comentarios -->
            <input type="text" name="pais_gb_ja" value="<?php echo $grupobsEditar->pais_gb_ja; ?>"
            class="form-control" placeholder="Ingrese el nombre del Pais" required>
          </div>
        </div>
        <br>
        <div class="row">
          <div class="col-md-4 text-right">
            <label for=""> Goles a favor </label>
          </div>
          <div class="col-md-7">
            <!-- el tipo de comentarios -->
            <input type="text" name="favor_gb_ja" value="<?php echo $grupobsEditar->favor_gb_ja; ?>"
            class="form-control" placeholder="Ingrese goles a favor " required>
          </div>
        </div>
        <br>
        <div class="row">
          <div class="col-md-4 text-right">
            <label for=""> Goles en contra </label>
          </div>
          <div class="col-md-7">
            <!-- el tipo de comentarios -->
            <input type="text" name="contra_gb_ja" value="<?php echo $grupobsEditar->contra_gb_ja; ?>"
            class="form-control" placeholder="Ingrese goles encontra " required>
          </div>
        </div>
        <br>
        <div class="row">
          <div class="col-md-4 text-right">
            <label for="">Goles de diferencia </label>
          </div>
          <div class="col-md-7">
            <!-- el tipo de comentarios -->
            <input type="text" name="diferencia_gb_ja" value="<?php echo $grupobsEditar->diferencia_gb_ja; ?>"
            class="form-control" placeholder="Ingrese la diferencia " required>
          </div>
        </div>
        <br>

        <div class="row">
          <div class="col-md-4 text-right">
            <label for=""> puntos  </label>
          </div>
          <div class="col-md-7">
            <!-- el tipo de comentarios -->
            <input type="text" name="puntos_gb_ja" value="<?php echo $grupobsEditar->puntos_gb_ja; ?>"
            class="form-control" placeholder="Ingrese los puntos " required>
          </div>
        </div>
        <br>

      <div class="row">
        <div class="col-md-4">
        </div>
        <div class="col-md-7">
          <!-- cambianos el nombre de button a submit -->
          <button type="submit" name="button"
                class="btn btn-warning">
              <i class="glyphicon glyphicon-ok"></i>
              ACTUALIZAR
          </button>
          <a href="<?php echo site_url('grupobs/index') ?>" class="btn btn-danger">
            <i class="glyphicon glyphicon-remove"></i>
            CANCELAR
          </a>
        </div>
      </div>
      </form>

    <?php else: ?>
      <div class="alert alert-danger">
        <b>NO SE ENCONTRO NUEVOS INGRESOS  </b>
      </div>
    <?php endif; ?>
  </div>
</div>
