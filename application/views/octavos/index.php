<legend class="text-center">
  <!-- gestion de estudiantes -->
  <i class="glyphicon glyphicon-user" style="background-color: #E4F4E9;" ></i>
<b>  LISTADO DE OCTAVOS </b><br>
<br>

</legend>
<center>
  <a href="<?php echo site_url('octavos/nuevo'); ?>" class="btn btn-success">
    <i class="glyphicon glyphicon-plus"></i>
    Agregar Nuevo
  </a>
</center>
<hr>
<?php if ($listadoOctavos): ?>
  <table id = "tbl-octavos"class="table table-striped table-bordered table-hover">
    <thead>
      <tr>
        <th class="text-center"> ID</th>
        <th class="text-center"> PAIS </th>
        <th class="text-center">GRUPO</th>
        <th class="text-center">GOLES</th>
        <th class="text-center"> FOTO</th>
        <th class="text-center"> ACCIONES</th>

      </tr>

    </thead>

    <tbody>
      <!-- $listadoClientes->result() as $clienteTemporal -->
        <?php foreach ($listadoOctavos->result() as $octavoTemporal): ?>
          <tr>
            <td class="text-center"><?php echo $octavoTemporal->id_oc_ja; ?></td>
            <td class="text-center"><?php echo $octavoTemporal->pais_oc_ja;?></td>
            <td class="text-center"><?php echo $octavoTemporal->grupo_oc_ja; ?></td>
            <td class="text-center"><?php echo $octavoTemporal->goles_oc_ja; ?></td>

            <td class="text-center">
              <?php if ($octavoTemporal->foto_oc_ja!=""): ?>
                      <a href="<?php echo base_url('uploads/octavos').'/'.$octavoTemporal->foto_oc_ja; ?>"
                        target="_blank">
                        <img src="<?php echo base_url('uploads/octavos').'/'.$octavoTemporal->foto_oc_ja; ?>"
                        width="50px" height="50px"
                        alt="">
                      </a>
                    <?php else: ?>
                      N/A
                    <?php endif; ?>
              </td>
              <!-- ruta <?php echo base_url('uploads/octavos/').'/'.$octavoTemporal->foto_oc_ja ?> -->
            <td class="text-center">
              <a href="<?php echo site_url('octavos/actualizar'); ?>/<?php echo $octavoTemporal->id_oc_ja ?>" class="btn btn-warning">
                <i class="glyphicon glyphicon-edit"></i>
                Editar
              </a>
              <a href="<?php echo site_url('octavos/borrar'); ?>/<?php echo $octavoTemporal->id_oc_ja?>" class="btn btn-danger" onclick="return confirm('esta seguro de eliminar?');">
                <i class="glyphicon glyphicon-trash"></i>
                Eliminar
              </a>
            </td>
          </tr>
        <?php endforeach; ?>

    </tbody>

  </table>
<?php else: ?>
  <h3><b>No existen ningun grupo </b></h3>
<?php endif; ?>
<script type="text/javascript">
  $("#tbl-octavos").DataTable();
</script>

<br>
<br>
