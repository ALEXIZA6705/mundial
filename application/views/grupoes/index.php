<legend class="text-center">
  <!-- gestion de estudiantes -->
  <i class="glyphicon glyphicon-user" style="background-color: #e3f2fd;" ></i>
<b>  LISTADO DE GRUPO  E </b><br>
<br>

</legend>
<center>
  <a href="<?php echo site_url('grupoes/nuevo'); ?>" class="btn btn-success">
    <i class="glyphicon glyphicon-plus"></i>
    Agregar Nuevo Equipo
  </a>
</center>
<hr>
<?php if ($listadoGrupoes): ?>
  <table id = "tbl-grupoes"class="table table-striped table-bordered table-hover">
    <thead>
      <tr>
        <th class="text-center"> ID</th>
        <th class="text-center"> NUMERO</th>
        <th class="text-center"> PAIS </th>
        <th class="text-center">FAVOR</th>
        <th class="text-center">CONTRA</th>
        <th class="text-center">DIFERENCIA</th>
        <th class="text-center">PUNTOS</th>
        <th class="text-center"> FOTO</th>
        <th class="text-center"> ACCIONES</th>

      </tr>

    </thead>

    <tbody>
      <!-- $listadoClientes->result() as $clienteTemporal -->
        <?php foreach ($listadoGrupoes->result() as $grupoeTemporal): ?>
          <tr>
            <td class="text-center"><?php echo $grupoeTemporal->id_ge_ja; ?></td>
            <td class="text-center"><?php echo $grupoeTemporal->numero_ge_ja;?></td>
            <td class="text-center"><?php echo $grupoeTemporal->pais_ge_ja;?></td>
            <td class="text-center"><?php echo $grupoeTemporal->favor_ge_ja; ?></td>
            <td class="text-center"><?php echo $grupoeTemporal->contra_ge_ja; ?></td>
            <td class="text-center"><?php echo $grupoeTemporal->diferencia_ge_ja; ?></td>
            <td class="text-center"><?php echo $grupoeTemporal->puntos_ge_ja; ?></td>

            <td class="text-center">
              <?php if ($grupoeTemporal->foto_ge_ja!=""): ?>
                      <a href="<?php echo base_url('uploads/grupoes').'/'.$grupoeTemporal->foto_ge_ja; ?>"
                        target="_blank">
                        <img src="<?php echo base_url('uploads/grupoes').'/'.$grupoeTemporal->foto_ge_ja; ?>"
                        width="50px" height="50px"
                        alt="">
                      </a>
                    <?php else: ?>
                      N/A
                    <?php endif; ?>
              </td>
              <!-- ruta <?php echo base_url('uploads/grupoes/').'/'.$grupoeTemporal->foto_ge_ja ?> -->
            <td class="text-center">
              <a href="<?php echo site_url('grupoes/actualizar'); ?>/<?php echo $grupoeTemporal->id_ge_ja ?>" class="btn btn-warning">
                <i class="glyphicon glyphicon-edit"></i>
                Editar
              </a>
              <a href="<?php echo site_url('grupoes/borrar'); ?>/<?php echo $grupoeTemporal->id_ge_ja?>" class="btn btn-danger" onclick="return confirm('esta seguro de eliminar?');">
                <i class="glyphicon glyphicon-trash"></i>
                Eliminar
              </a>
            </td>
          </tr>
        <?php endforeach; ?>

    </tbody>

  </table>
<?php else: ?>
  <h3><b>No existen ningun grupo </b></h3>
<?php endif; ?>
<script type="text/javascript">
  $("#tbl-grupoes").DataTable();
</script>

<br>
<br>
