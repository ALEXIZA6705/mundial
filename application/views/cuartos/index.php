<legend class="text-center">
  <!-- gestion de estudiantes -->
  <i class="glyphicon glyphicon-user" style="background-color: #E4F4E9;" ></i>
<b>  LISTADO DE cuartos </b><br>
<br>

</legend>
<center>
  <a href="<?php echo site_url('cuartos/nuevo'); ?>" class="btn btn-success">
    <i class="glyphicon glyphicon-plus"></i>
    Agregar Nuevo
  </a>
</center>
<hr>
<?php if ($listadoCuartos): ?>
  <table id = "tbl-cuartos"class="table table-striped table-bordered table-hover">
    <thead>
      <tr>
        <th class="text-center"> ID</th>
        <th class="text-center"> PAIS </th>
        <th class="text-center">GRUPO</th>
        <th class="text-center">GOLES</th>
        <th class="text-center"> FOTO</th>
        <th class="text-center"> ACCIONES</th>

      </tr>

    </thead>

    <tbody>
      <!-- $listadoClientes->result() as $clienteTemporal -->
        <?php foreach ($listadoCuartos->result() as $cuartoTemporal): ?>
          <tr>
            <td class="text-center"><?php echo $cuartoTemporal->id_cu_ja; ?></td>
            <td class="text-center"><?php echo $cuartoTemporal->pais_cu_ja;?></td>
            <td class="text-center"><?php echo $cuartoTemporal->grupo_cu_ja; ?></td>
            <td class="text-center"><?php echo $cuartoTemporal->goles_cu_ja; ?></td>

            <td class="text-center">
              <?php if ($cuartoTemporal->foto_cu_ja!=""): ?>
                      <a href="<?php echo base_url('uploads/cuartos').'/'.$cuartoTemporal->foto_cu_ja; ?>"
                        target="_blank">
                        <img src="<?php echo base_url('uploads/cuartos').'/'.$cuartoTemporal->foto_cu_ja; ?>"
                        width="50px" height="50px"
                        alt="">
                      </a>
                    <?php else: ?>
                      N/A
                    <?php endif; ?>
              </td>
              <!-- ruta <?php echo base_url('uploads/cuartos/').'/'.$cuartoTemporal->foto_cu_ja ?> -->
            <td class="text-center">
              <a href="<?php echo site_url('cuartos/actualizar'); ?>/<?php echo $cuartoTemporal->id_cu_ja ?>" class="btn btn-warning">
                <i class="glyphicon glyphicon-edit"></i>
                Editar
              </a>
              <a href="<?php echo site_url('cuartos/borrar'); ?>/<?php echo $cuartoTemporal->id_cu_ja?>" class="btn btn-danger" onclick="return confirm('esta seguro de eliminar?');">
                <i class="glyphicon glyphicon-trash"></i>
                Eliminar
              </a>
            </td>
          </tr>
        <?php endforeach; ?>

    </tbody>

  </table>
<?php else: ?>
  <h3><b>No existen ningun grupo </b></h3>
<?php endif; ?>
<script type="text/javascript">
  $("#tbl-cuartos").DataTable();
</script>

<br>
<br>
