<div class="row">
  <div class="col-md-12 text-center well">
    <h3>ACTUALIZAR cuarto </h3>
    <center>
      <a href="<?php echo site_url('cuartos/index') ?>" class="btn btn-success">
      <i class="glyphicon glyphicon-repeat"></i>
      REGRESAR
      </a>
    </center>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <?php if ($cuartosEditar): ?>
      <!-- <?php print_r($cuartosEditar); ?> -->
      <!-- copiamos el formulario para haorrarnos tiempo -->
      <form  class="" action="<?php echo site_url('cuartos/procesarActualizacion') ?>" method="post">
        <center>
          <input type="text" name="id_cu_ja" value="<?php echo $cuartosEditar->id_cu_ja; ?> "></input>
          <!-- //hidden sirve para ocultar  el formulario -->
        </center>


        <div class="row">
          <div class="col-md-4 text-right">
            <label for="">Pais</label>
          </div>
          <div class="col-md-7">
            <!-- el tipo de comentarios -->
            <input type="text" name="pais_cu_ja" value="<?php echo $cuartosEditar->pais_cu_ja; ?>"
            class="form-control" placeholder="Ingrese el nombre del Pais" required>
          </div>
        </div>
        <br>
        <div class="row">
          <div class="col-md-4 text-right">
            <label for="">Grupo  </label>
          </div>
          <div class="col-md-7">
            <!-- el tipo de comentarios -->
            <input type="text" name="grupo_cu_ja" value="<?php echo $cuartosEditar->grupo_cu_ja; ?>"
            class="form-control" placeholder="Ingrese el grupo " required>
          </div>
        </div>
        <br>
        <div class="row">
          <div class="col-md-4 text-right">
            <label for=""> Goles  </label>
          </div>
          <div class="col-md-7">
            <!-- el tipo de comentarios -->
            <input type="text" name="goles_cu_ja" value="<?php echo $cuartosEditar->goles_cu_ja; ?>"
            class="form-control" placeholder="Ingrese goles  " required>
          </div>
        </div>
        <br>


      <div class="row">
        <div class="col-md-4">
        </div>
        <div class="col-md-7">
          <!-- cambianos el nombre de button a submit -->
          <button type="submit" name="button"
                class="btn btn-warning">
              <i class="glyphicon glyphicon-ok"></i>
              ACTUALIZAR
          </button>
          <a href="<?php echo site_url('cuartos/index') ?>" class="btn btn-danger">
            <i class="glyphicon glyphicon-remove"></i>
            CANCELAR
          </a>
        </div>
      </div>
      </form>

    <?php else: ?>
      <div class="alert alert-danger">
        <b>NO SE ENCONTRO NUEVOS INGRESOS  </b>
      </div>
    <?php endif; ?>
  </div>
</div>
