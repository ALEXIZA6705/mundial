<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <!-- importacion para dispocitivos moviles responsibilidad  -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>MUNDIAL</title>
    <!-- IMPORTACION JQUERY -->
    <script src="https://code.jquery.com/jquery-3.6.1.min.js" integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ=" crossorigin="anonymous"></script>
      <!-- FIN DE IMPORTACION  -->
      <!-- importacion de jquery validate  -->
      <script type="text/javascript" src="<?php echo base_url('assets/librerias/validate/jquery.validate.min.js'); ?>"> </script>
      <!-- importacion de adicional metodos -->
        <script type="text/javascript" src="<?php echo base_url('assets/librerias/validate/additional-methods.min.js'); ?>"> </script>
        <!-- importacion de message -->
        <script type="text/javascript" src="<?php echo base_url('assets/librerias/validate/messages_es_AR.min.js'); ?>"> </script>
          <br>
      <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <!-- importacion sweetalert2 -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/11.6.15/sweetalert2.css" integrity="sha512-JzSVRb7c802/njMbV97pjo1wuJAE/6v9CvthGTDxiaZij/TFpPQmQPTcdXyUVucsvLtJBT6YwRb5LhVxX3pQHQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/11.6.15/sweetalert2.js" integrity="sha512-9V+5wAdU/RmYn1TP+MbEp5Qy9sCDYmvD2/Ub8sZAoWE2o6QTLsKx/gigfub/DlOKAByfhfxG5VKSXtDlWTcBWQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <?php if ($this->session->flashdata('confirmacion')): ?>
      <script type="text/javascript">
      Swal.fire(
    'CONFIRMACION',
    ' guardado exitosamnete ',
    'success'
  )

      </script>
    <?php endif; ?>

  </head>
  <body background="<?php echo base_url(); ?>/assets/images/m7.jpg" >
    <div class="text-rigth">
        <img src="<?php echo base_url(); ?>/assets/images/2022.gif"  alt="logo" height="180px">

  </div>

<!-- CONTENIDO -->
    <nav class="navbar navbar-light" style="background-color: #e3f2fd;">
  <div class="container-fluid">

    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">

      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?php echo site_url(); ?>">
        MUNDIAL</a>
    </div>


    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <!--<li class="active"><a href="#">Link <span class="sr-only">(current)</span></a></li>-->
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Equipos <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="<?php echo site_url('equipos/index') ?>">Listado</a></li>
            <li><a href="<?php echo site_url('equipos/nuevo') ?>">Nuevo</a></li>

          </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">GruposA <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="<?php echo site_url('grupoas/index') ?>">Listado</a></li>
            <li><a href="<?php echo site_url('grupoas/nuevo') ?>">Nuevo</a></li>

          </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">GruposB <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="<?php echo site_url('grupobs/index') ?>">Listado</a></li>
            <li><a href="<?php echo site_url('grupobs/nuevo') ?>">Nuevo</a></li>

          </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">GruposC <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="<?php echo site_url('grupocs/index') ?>">Listado</a></li>
            <li><a href="<?php echo site_url('grupocs/nuevo') ?>">Nuevo</a></li>

          </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">GruposD <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="<?php echo site_url('grupods/index') ?>">Listado</a></li>
            <li><a href="<?php echo site_url('grupods/nuevo') ?>">Nuevo</a></li>

          </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">GruposE <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="<?php echo site_url('grupoes/index') ?>">Listado</a></li>
            <li><a href="<?php echo site_url('grupoes/nuevo') ?>">Nuevo</a></li>

          </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">GruposF <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="<?php echo site_url('grupofs/index') ?>">Listado</a></li>
            <li><a href="<?php echo site_url('grupofs/nuevo') ?>">Nuevo</a></li>

          </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">GruposG <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="<?php echo site_url('grupogs/index') ?>">Listado</a></li>
            <li><a href="<?php echo site_url('grupogs/nuevo') ?>">Nuevo</a></li>

          </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">GruposH <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="<?php echo site_url('grupohs/index') ?>">Listado</a></li>
            <li><a href="<?php echo site_url('grupohs/nuevo') ?>">Nuevo</a></li>

          </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Octavos <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="<?php echo site_url('octavos/index') ?>">Listado</a></li>
            <li><a href="<?php echo site_url('octavos/nuevo') ?>">Nuevo</a></li>

          </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">cuartos  <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="<?php echo site_url('cuartos/index') ?>">Listado</a></li>
            <li><a href="<?php echo site_url('cuartos/nuevo') ?>">Nuevo</a></li>

          </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Semifinal  <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="<?php echo site_url('semifinales/index') ?>">Listado</a></li>
            <li><a href="<?php echo site_url('semifinales/nuevo') ?>">Nuevo</a></li>

          </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Final  <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="<?php echo site_url('finales/index') ?>">Listado</a></li>
            <li><a href="<?php echo site_url('finales/nuevo') ?>">Nuevo</a></li>

          </ul>
        </li>






      </ul>

    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
