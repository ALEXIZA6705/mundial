<legend class="text-center">
  <!-- gestion de estudiantes -->
  <i class="glyphicon glyphicon-user"></i>
<b>  Gestion de Equipos</b><br>
<br>

</legend>
<center>
  <a href="<?php echo site_url('equipos/nuevo'); ?>" class="btn btn-success">
    <i class="glyphicon glyphicon-plus"></i>
    Agregar Nuevo
  </a>
</center>
<hr>
<?php if ($listadoEquipos): ?>
  <table id = "tbl-estudiantes"class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
      <th class="text-center"> ID</th>
      <th class="text-center"> CONTINENTE</th>
      <th class="text-center"> PAIS</th>
      <th class="text-center"> RANKING</th>
      <th class="text-center">FOTO</th>
      <th class="text-center"> ACCIONES</th>

    </tr>

    </thead>

      <tbody>
      <!-- $listadoClientes->result() as $clienteTemporal -->
      <?php foreach ($listadoEquipos->result() as $equipoTemporal): ?>
        <tr>
          <td class="text-center"><?php echo $equipoTemporal->id_ja; ?></td>
          <td class="text-center"><?php echo $equipoTemporal->continente_ja ?></td>
          <td class="text-center"><?php echo $equipoTemporal->pais_ja?></td>
          <td class="text-center"><?php echo $equipoTemporal->ranking_ja ?></td>
          <td class="text-center">
            <?php if ($equipoTemporal->foto_ja!=""): ?>
                    <a href="<?php echo base_url('uploads/equipos').'/'.$equipoTemporal->foto_ja; ?>"
                      target="_blank">
                      <img src="<?php echo base_url('uploads/equipos').'/'.$equipoTemporal->foto_ja; ?>"
                      width="50px" height="50px"
                      alt="">
                    </a>
                  <?php else: ?>
                    N/A
                  <?php endif; ?>
            </td>
            <!-- ruta <?php echo base_url('uploads/equipos/').'/'.$equipoTemporal->foto_ja ?> -->
          <td class="text-center">
            <a href="<?php echo site_url('equipos/actualizar'); ?>/<?php echo $equipoTemporal->id_ja ?>" class="btn btn-warning">
              <i class="glyphicon glyphicon-edit"></i>
              Editar
            </a>
            <a href="<?php echo site_url('equipos/borrar'); ?>/<?php echo $equipoTemporal->id_ja ?>" class="btn btn-danger" onclick="return confirm('esta seguro de eliminar?');">
              <i class="glyphicon glyphicon-trash"></i>
              Eliminar
            </a>
          </td>
        </tr>
      <?php endforeach; ?>
    </tbody>

  </table>
<?php else: ?>
  <h3><b>No existen equipos</b></h3>
<?php endif; ?>
<script type="text/javascript">
  $("#tbl-equipos").DataTable();
</script>

<br>
<br>
