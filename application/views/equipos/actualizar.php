<div class="row">
  <div class="col-md-12 text-center well">
    <h3>ACTUALIZAR EQUIPOS</h3>
    <center>
      <a href="<?php echo site_url('equipos/index') ?>" class="btn btn-success">
      <i class="glyphicon glyphicon-repeat"></i>
      REGRESAR
      </a>
    </center>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <?php if ($equiposEditar): ?>
      <!-- <?php print_r($equiposEditar); ?> -->
      <!-- copiamos el formulario para haorrarnos tiempo -->
      <form  class="" action="<?php echo site_url('equipos/procesarActualizacion') ?>" method="post">
        <center>
          <input type="text" name="id_ja" value="<?php echo $equiposEditar->id_ja; ?> "></input>
          <!-- //hidden sirve para ocultar  el formulario -->
        </center>
        <br>
        <div class="row">
          <div class="col-md-4 text-right">
            <label for="">Continente</label>
          </div>
          <div class="col-md-7">
            <input type="text" name="continente_ja" value="<?php echo $equiposEditar->continente_ja; ?>"
            class="form-control" placeholder="Ingrese el nombre del continente " required>
          </div>
        </div>
        <br>
        <div class="row">
          <div class="col-md-4 text-right">
            <label for="">Pais</label>
          </div>
          <div class="col-md-7">
            <!-- el tipo de comentarios -->
            <input type="text" name="pais_ja" value="<?php echo $equiposEditar->pais_ja; ?>"
            class="form-control" placeholder="Ingrese el nombre del Pais" required>
          </div>
        </div>
        <br>
        <div class="row">
          <div class="col-md-4 text-right">
            <label for="">Ranking</label>
          </div>
          <div class="col-md-7">
            <!-- el tipo de comentarios -->
            <input type="text" name="ranking_ja" value="<?php echo $equiposEditar->ranking_ja; ?>"
            class="form-control" placeholder="Ingrese el puesto de clasificacion mundial" required>
          </div>
        </div>
        <br
        <br>
        <div class="row">
          <div class="col-md-4 text-right">
            <label for="">Fotografia</label>
          </div>
          <div class="col-md-7">
            <!-- el tipo de comentarios -->
            <input type="text" name="foto_ja" value="<?php echo $equiposEditar->foto_ja; ?>"
            class="form-control" placeholder="Ingrese el puesto de clasificacion mundial" required>
          </div>
        </div>
        <br>

      <div class="row">
        <div class="col-md-4">
        </div>
        <div class="col-md-7">
          <!-- cambianos el nombre de button a submit -->
          <button type="submit" name="button"
                class="btn btn-warning">
              <i class="glyphicon glyphicon-ok"></i>
              ACTUALIZAR
          </button>
          <a href="<?php echo site_url('equipos/index') ?>" class="btn btn-danger">
            <i class="glyphicon glyphicon-remove"></i>
            CANCELAR
          </a>
        </div>
      </div>
      </form>

    <?php else: ?>
      <div class="alert alert-danger">
        <b>NO SE ENCONTRO EL EQUIPO </b>
      </div>
    <?php endif; ?>
  </div>
</div>
