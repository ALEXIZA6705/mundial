<legend class="text-center">
  <i class="glyphicon glyphicon-plus"></i>
  NUEVO EQUIPOS
</legend>
<form id="frm_nuevo_equipo"class=""
enctype="multipart/form-data"
action="<?php echo site_url('equipos/guardarEquipo'); ?>" method="post">

<br>
<div class="row">
  <div class="col-md-4 text-right">
    <label for="">Continente:</label>
  </div>
  <div class="col-md-7">
    <input type="text" id="continente_ja"name="continente_ja" value=""class="form-control"placeholder="Ingrese el nombre del continente " >
  </div>
</div>

<br>
<div class="row">
  <div class="col-md-4 text-right">
    <label for="">Pais:</label>
  </div>
  <div class="col-md-7">
    <input type="text" id="pais_ja" name="pais_ja" value=""class="form-control"placeholder="Ingrese el nombre del pais  " required >
  </div>
</div>
<br>
<div class="row">
  <div class="col-md-4 text-right">
    <label for="">Ranking:</label>
  </div>
  <div class="col-md-7">
    <input type="text" id="ranking_ja" name="ranking_ja" value=""class="form-control"placeholder="Ingrese el puesto global en el que se encuentra"required>
  </div>
</div>
<br>
<div class="row">
  <div class="col-md-4 text-right">
    <label for="">Fotografia :</label>
  </div>
  <div class="col-md-7">
    <input type="file" id="foto_ja" name="foto_ja" value="required" accept="image/*"class="form-control"placeholder=" seleccione una foto ">
  </div>
</div>
<br>
<br>
<div class="row">
  <div class="col-md-4">
  </div>
  <div class="col-md-7">
    <button type="submit" name="button" class="btn btn-primary">
<i class="glyphicon glyphicon-ok"></i>
Guardar
    </button>
    <a href="<?php echo site_url('equipos/index'); ?>" class="btn btn-danger">
<i class="glyphicon glyphicon-remove"></i>
Cancelar
    </a>
  </div>

</div>

</form>
<script type="text/javascript">
     $("#frm_nuevo_equipo").validate({
        rules:{
            continente_ja:{
              required:true,
              minlength:10,
              maxlength:10,
              digits:true
            },
            pais_ja:{
              required:true,
              minlength:10,
              maxlength:10,
              digits:true
            },
            ranking_ja:{
              required:true,
              minlength:3
            }


        },
        messages:{
            continente_ja:{
              required:"Por el nombre del continente",
              minlength:"continente incorrecto"
            },

            pais_ja:{
              required:"Por favor ingrese el nombre del pais",
              minlength:"Nombre incorrecto"
            },
            ranking_ja:{
              required:"Por favor ingrese el ranking global",
              minlength:"numero incorrecto",
              maxlength:"numero incorrecto",
              digits:"Este campo solo acepta números"
            }


        },
     });
</script>
