<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Finales extends CI_Controller {
	public function __construct(){
		parent::__construct();


		$this->load->model("final");
	}
// renderiza la vista estudiantes
	public function index()
	{

		$data["listadoFinales"]=$this->final->obtenerTodos();
		$this->load->view('header');

		$this->load->view('finales/index',$data);
		$this->load->view('footer');
	}
// renderiza la vista de nuevo
  	public function nuevo()
  	{

  		$this->load->view('header');

  		$this->load->view('finales/nuevo');
  		$this->load->view('footer');
  	}
		// funcion para capturar los datos del formulario
		public function guardarFinal(){
        $datosNuevoFinal=array(


          "foto_fin_ja"=>$this->input->post("foto_fin_ja"),
          "pais_fin_ja"=>$this->input->post("pais_fin_ja"),
					"grupo_fin_ja"=>$this->input->post("grupo_fin_ja"),
        	"goles_fin_ja"=>$this->input->post("goles_fin_ja")

        );

				// INICIO DE PROCESO DE SUBIDA DE FOTOGRAFIA
				$this->load->library("upload");// activando la liibreria de subida de archivos
		    $new_name = "foto_" . time() . "_" . rand(1, 5000);//generando un nombre aleatorio
		    $config['file_name'] = $new_name;

		    $config['upload_path'] = FCPATH . 'uploads/finales/';// ruta de subida
		    $config['allowed_types'] = 'jpg|png';//pdf|docx
		    $config['max_size']  = 5*1024;//5MB
		    $this->upload->initialize($config);// inicializar la configuracion
				// validando la subida del aarchivo
		    if ($this->upload->do_upload("foto_fin_ja")) {
		      $dataSubida = $this->upload->data();
		      $datosNuevoFinal["foto_fin_ja"] = $dataSubida['file_name'];
		    }
				// FIN DEL PROCESO DE ARCHIVOS


				print_r($datosNuevoFinal);
				if ($this->final->insertar($datosNuevoFinal)) {
									 				$this->session->set_flashdata('confirmacion','insertado exitosamente');
									 				}else	{
									 			$this->session->set_flashdata('error','verifique e intente de nuevo');
									 				}
									 					redirect('finales/index');
									       }


	public function borrar($id_fin_ja){

					if($this->final->eliminarPorId($id_fin_ja)){
						$this->session->set_flashdata('confirmacion','estudiante eliminado exitosamente');
	            }else{
	            $this->session->set_flashdata('error','verifique e intente nuevamnete ');
	            }
							redirect('finales/index');
	        }

			//funcion para renderizar eliminar estudiantes
			// ACTUALIZAR
			public function actualizar ($id){
				$data["finalesEditar"]=$this->final->obtenerporId($id);
				$this->load->view("header");
				$this->load->view("finales/actualizar",$data);
				$this->load->view("footer");
			}

			public function procesarActualizacion(){
			$id_fin_ja=$this->input->post("id_fin_ja");
			$datosFinalEditado=array(
				"foto_fin_ja"=>$this->input->post("foto_fin_ja"),
				"pais_fin_ja"=>$this->input->post("pais_fin_ja"),
				"grupo_fin_ja"=>$this->input->post("grupo_fin_ja"),
				"goles_fin_ja"=>$this->input->post("goles_fin_ja")
			);
			if($this->final->actualizar($id_fin_ja,$datosFinalEditado)){
			}else{

			}
			redirect('finales/index');

		}
}
?>
