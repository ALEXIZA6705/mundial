<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Grupoas extends CI_Controller {
	public function __construct(){
		parent::__construct();

		$this->load->model("grupoa");
	}
// renderiza la vista estudiantes
	public function index()
	{
		$data["listadoGrupoas"]=$this->grupoa->obtenerTodos();
		$this->load->view('header');
		$this->load->view('grupoas/index',$data);
		$this->load->view('footer');
	}
// renderiza la vista de nuevo
  	public function nuevo()
  	{

  		$this->load->view('header');
  		$this->load->view('grupoas/nuevo');
  		$this->load->view('footer');
  	}
		// funcion para capturar los datos del formulario
		public function guardarGrupoa(){
        $datosNuevoGrupoa=array(
          "numero_ga_ja"=>$this->input->post("numero_ga_ja"),
          "foto_ga_ja"=>$this->input->post("foto_ga_ja"),
          "pais_ga_ja"=>$this->input->post("pais_ga_ja"),
					"favor_ga_ja"=>$this->input->post("favor_ga_ja"),
          "contra_ga_ja"=>$this->input->post("contra_ga_ja"),
					"diferencia_ga_ja"=>$this->input->post("diferencia_ga_ja"),
					"puntos_ga_ja"=>$this->input->post("puntos_ga_ja")

        );

				// INICIO DE PROCESO DE SUBIDA DE FOTOGRAFIA

						$this->load->library("upload"); //activando libreria de subida de archivos
				    	$new_name = "fotO_" . time() . "_" . rand(1, 5000);//generanado un nombre aleatorio
				    	$config['file_name'] = $new_name;
				    	$config['upload_path'] = FCPATH . 'uploads/grupoas'; //ruta donde vamos a subir el archivo
				    	$config['allowed_types'] = 'jpg|png'; //tipo de archivos permitidos  'pdf|word'
						$config['max_size'] = 2 * 1024; //tamano de la imagen 5mb
				    	$this->upload->initialize($config);
				    	if ($this->upload->do_upload("foto_ga_ja")) {
							//que se suba con exito
				      	$dataSubida = $this->upload->data();
						$datosNuevoGrupoa["foto_ga_ja"] = $dataSubida['file_name'];
				    }

				// FIN DEL PROCESO DE ARCHIVOS


		print_r($datosNuevoGrupoa);
				if ($this->grupoa->insertar($datosNuevoGrupoa)) {
					$this->session->set_flashdata('confirmacion',' insertado exitosamente');
						}else	{
					$this->session->set_flashdata('error','verifique e intente de nuevo');
										}
					redirect('grupoas/index');
											  }

	public function borrar($id_ga_ja){

					if($this->grupoa->eliminarPorId($id_ga_ja)){
						$this->session->set_flashdata('confirmacion','estudiante eliminado exitosamente');
	            }else{
	            $this->session->set_flashdata('error','verifique e intente nuevamnete ');
	            }
							redirect('grupoas/index');
	        }

			//funcion para renderizar eliminar estudiantes
			// ACTUALIZAR
			public function actualizar ($id){
				$data["grupoasEditar"]=$this->grupoa->obtenerporId($id);
				$this->load->view("header");
				$this->load->view("grupoas/actualizar",$data);
				$this->load->view("footer");
			}

			public function procesarActualizacion(){
			$id_ga_ja=$this->input->post("id_ga_ja");
			$datosGrupoaEditado=array(
				"numero_ga_ja"=>$this->input->post("numero_ga_ja"),
				"foto_ga_ja"=>$this->input->post("foto_ga_ja"),
				"pais_ga_ja"=>$this->input->post("pais_ga_ja"),
				"favor_ga_ja"=>$this->input->post("favor_ga_ja"),
				"contra_ga_ja"=>$this->input->post("contra_ga_ja"),
				"diferencia_ga_ja"=>$this->input->post("diferencia_ga_ja"),
				"puntos_ga_ja"=>$this->input->post("puntos_ga_ja")
			);
			if($this->grupoa->actualizar($id_ga_ja,$datosGrupoaEditado)){
			}else{

			}
			redirect('grupoas/index');

		}
}
?>
