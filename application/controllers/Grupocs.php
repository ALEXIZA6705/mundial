<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Grupocs extends CI_Controller {
	public function __construct(){
		parent::__construct();

		$this->load->model("grupoc");
	}
// renderiza la vista estudiantes
	public function index()
	{
		$data["listadoGrupocs"]=$this->grupoc->obtenerTodos();
		$this->load->view('header');
		$this->load->view('grupocs/index',$data);
		$this->load->view('footer');
	}
// renderiza la vista de nuevo
  	public function nuevo()
  	{

  		$this->load->view('header');
  		$this->load->view('grupocs/nuevo');
  		$this->load->view('footer');
  	}
		// funcion para capturar los datos del formulario
		public function guardarGrupoc(){
        $datosNuevoGrupoc=array(
          "numero_gc_ja"=>$this->input->post("numero_gc_ja"),
          "foto_gc_ja"=>$this->input->post("foto_gc_ja"),
          "pais_gc_ja"=>$this->input->post("pais_gc_ja"),
					"favor_gc_ja"=>$this->input->post("favor_gc_ja"),
          "contra_gc_ja"=>$this->input->post("contra_gc_ja"),
					"diferencia_gc_ja"=>$this->input->post("diferencia_gc_ja"),
					"puntos_gc_ja"=>$this->input->post("puntos_gc_ja")

        );

				// INICIO DE PROCESO DE SUBIDA DE FOTOGRAFIA
				$this->load->library("upload");// activando la liibreria de subida de archivos
		    $new_name = "foto_" . time() . "_" . rand(1, 5000);//generando un nombre aleatorio
		    $config['file_name'] = $new_name;
		    $config['upload_path'] = FCPATH . 'uploads/grupocs/';// ruta de subida
		    $config['allowed_types'] = 'jpg|png';//pdf|docx
		    $config['max_size']  = 5*1024;//5MB
		    $this->upload->initialize($config);// inicializar la configuracion
				// validando la subida del aarchivo
		    if ($this->upload->do_upload("foto_gc_ja")) {
		      $dataSubida = $this->upload->data();
		      $datosNuevoGrupoc["foto_gc_ja"] = $dataSubida['file_name'];
		    }
				// FIN DEL PROCESO DE ARCHIVOS


				print_r($datosNuevoGrupoc);
				if ($this->grupoc->insertar($datosNuevoGrupoc)) {
									 				$this->session->set_flashdata('confirmacion','insertado exitosamente');
									 				}else	{
									 			$this->session->set_flashdata('error','verifique e intente de nuevo');
									 				}
									 					redirect('grupocs/index');
									       }


	public function borrar($id_gc_ja){

					if($this->grupoc->eliminarPorId($id_gc_ja)){
						$this->session->set_flashdata('confirmacion','estudiante eliminado exitosamente');
	            }else{
	            $this->session->set_flashdata('error','verifique e intente nuevamnete ');
	            }
							redirect('grupocs/index');
	        }

			//funcion para renderizar eliminar estudiantes
			// ACTUALIZAR
			public function actualizar ($id){
				$data["grupocsEditar"]=$this->grupoc->obtenerporId($id);
				$this->load->view("header");
				$this->load->view("grupocs/actualizar",$data);
				$this->load->view("footer");
			}

			public function procesarActualizacion(){
			$id_gc_ja=$this->input->post("id_gc_ja");
			$datosGrupocEditado=array(
				"numero_gc_ja"=>$this->input->post("numero_gc_ja"),
				"foto_gc_ja"=>$this->input->post("foto_gc_ja"),
				"pais_gc_ja"=>$this->input->post("pais_gc_ja"),
				"favor_gc_ja"=>$this->input->post("favor_gc_ja"),
				"contra_gc_ja"=>$this->input->post("contra_gc_ja"),
				"diferencia_gc_ja"=>$this->input->post("diferencia_gc_ja"),
				"puntos_gc_ja"=>$this->input->post("puntos_gc_ja")
			);
			if($this->grupoc->actualizar($id_gc_ja,$datosGrupocEditado)){
			}else{

			}
			redirect('grupocs/index');

		}
}
?>
