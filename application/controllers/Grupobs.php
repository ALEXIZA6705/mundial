<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Grupobs extends CI_Controller {
	public function __construct(){
		parent::__construct();

		$this->load->model("grupob");
	}
// renderiza la vista estudiantes
	public function index()
	{
		$data["listadoGrupobs"]=$this->grupob->obtenerTodos();
		$this->load->view('header');
		$this->load->view('grupobs/index',$data);
		$this->load->view('footer');
	}
// renderiza la vista de nuevo
  	public function nuevo()
  	{

  		$this->load->view('header');
  		$this->load->view('grupobs/nuevo');
  		$this->load->view('footer');
  	}
		// funcion para capturar los datos del formulario
		public function guardarGrupob(){
        $datosNuevoGrupob=array(
          "numero_gb_ja"=>$this->input->post("numero_gb_ja"),
          "foto_gb_ja"=>$this->input->post("foto_gb_ja"),
          "pais_gb_ja"=>$this->input->post("pais_gb_ja"),
					"favor_gb_ja"=>$this->input->post("favor_gb_ja"),
          "contra_gb_ja"=>$this->input->post("contra_gb_ja"),
					"diferencia_gb_ja"=>$this->input->post("diferencia_gb_ja"),
					"puntos_gb_ja"=>$this->input->post("puntos_gb_ja")

        );

				// INICIO DE PROCESO DE SUBIDA DE FOTOGRAFIA
				$this->load->library("upload");// activando la liibreria de subida de archivos
		    $new_name = "foto_" . time() . "_" . rand(1, 5000);//generando un nombre aleatorio
		    $config['file_name'] = $new_name;
		    $config['upload_path'] = FCPATH . 'uploads/grupobs/';// ruta de subida
		    $config['allowed_types'] = 'jpg|png';//pdf|docx
		    $config['max_size']  = 5*1024;//5MB
		    $this->upload->initialize($config);// inicializar la configuracion
				// validando la subida del aarchivo
		    if ($this->upload->do_upload("foto_gb_ja")) {
		      $dataSubida = $this->upload->data();
		      $datosNuevoGrupob["foto_gb_ja"] = $dataSubida['file_name'];
		    }
				// FIN DEL PROCESO DE ARCHIVOS


				print_r($datosNuevoGrupob);
				if ($this->grupob->insertar($datosNuevoGrupob)) {
									 				$this->session->set_flashdata('confirmacion','insertado exitosamente');
									 				}else	{
									 			$this->session->set_flashdata('error','verifique e intente de nuevo');
									 				}
									 					redirect('grupobs/index');
									       }


	public function borrar($id_gb_ja){

					if($this->grupob->eliminarPorId($id_gb_ja)){
						$this->session->set_flashdata('confirmacion','estudiante eliminado exitosamente');
	            }else{
	            $this->session->set_flashdata('error','verifique e intente nuevamnete ');
	            }
							redirect('grupobs/index');
	        }

			//funcion para renderizar eliminar estudiantes
			// ACTUALIZAR
			public function actualizar ($id){
				$data["grupobsEditar"]=$this->grupob->obtenerporId($id);
				$this->load->view("header");
				$this->load->view("grupobs/actualizar",$data);
				$this->load->view("footer");
			}

			public function procesarActualizacion(){
			$id_gb_ja=$this->input->post("id_gb_ja");
			$datosGrupobEditado=array(
				"numero_gb_ja"=>$this->input->post("numero_gb_ja"),
				"foto_gb_ja"=>$this->input->post("foto_gb_ja"),
				"pais_gb_ja"=>$this->input->post("pais_gb_ja"),
				"favor_gb_ja"=>$this->input->post("favor_gb_ja"),
				"contra_gb_ja"=>$this->input->post("contra_gb_ja"),
				"diferencia_gb_ja"=>$this->input->post("diferencia_gb_ja"),
				"puntos_gb_ja"=>$this->input->post("puntos_gb_ja")
			);
			if($this->grupob->actualizar($id_gb_ja,$datosGrupobEditado)){
			}else{

			}
			redirect('grupobs/index');

		}
}
?>
