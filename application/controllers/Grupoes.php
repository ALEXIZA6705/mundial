<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Grupoes extends CI_Controller {
	public function __construct(){
		parent::__construct();

		$this->load->model("grupoe");
	}
// renderiza la vista estudiantes
	public function index()
	{
		$data["listadoGrupoes"]=$this->grupoe->obtenerTodos();
		$this->load->view('header');
		$this->load->view('grupoes/index',$data);
		$this->load->view('footer');
	}
// renderiza la vista de nuevo
  	public function nuevo()
  	{

  		$this->load->view('header');
  		$this->load->view('grupoes/nuevo');
  		$this->load->view('footer');
  	}
		// funcion para capturar los datos del formulario
		public function guardarGrupoe(){
        $datosNuevoGrupoe=array(
          "numero_ge_ja"=>$this->input->post("numero_ge_ja"),
          "foto_ge_ja"=>$this->input->post("foto_ge_ja"),
          "pais_ge_ja"=>$this->input->post("pais_ge_ja"),
					"favor_ge_ja"=>$this->input->post("favor_ge_ja"),
          "contra_ge_ja"=>$this->input->post("contra_ge_ja"),
					"diferencia_ge_ja"=>$this->input->post("diferencia_ge_ja"),
					"puntos_ge_ja"=>$this->input->post("puntos_ge_ja")

        );

				// INICIO DE PROCESO DE SUBIDA DE FOTOGRAFIA
				$this->load->library("upload");// activando la liibreria de subida de archivos
		    $new_name = "foto_" . time() . "_" . rand(1, 5000);//generando un nombre aleatorio
		    $config['file_name'] = $new_name;
		    $config['upload_path'] = FCPATH . 'uploads/grupoes/';// ruta de subida
		    $config['allowed_types'] = 'jpg|png';//pdf|docx
		    $config['max_size']  = 5*1024;//5MB
		    $this->upload->initialize($config);// inicializar la configuracion
				// validando la subida del aarchivo
		    if ($this->upload->do_upload("foto_ge_ja")) {
		      $dataSubida = $this->upload->data();
		      $datosNuevoGrupoe["foto_ge_ja"] = $dataSubida['file_name'];
		    }
				// FIN DEL PROCESO DE ARCHIVOS


				print_r($datosNuevoGrupoe);
				if ($this->grupoe->insertar($datosNuevoGrupoe)) {
									 				$this->session->set_flashdata('confirmacion','insertado exitosamente');
									 				}else	{
									 			$this->session->set_flashdata('error','verifique e intente de nuevo');
									 				}
									 					redirect('grupoes/index');
									       }


	public function borrar($id_ge_ja){

					if($this->grupoe->eliminarPorId($id_ge_ja)){
						$this->session->set_flashdata('confirmacion','estudiante eliminado exitosamente');
	            }else{
	            $this->session->set_flashdata('error','verifique e intente nuevamnete ');
	            }
							redirect('grupoes/index');
	        }

			//funcion para renderizar eliminar estudiantes
			// ACTUALIZAR
			public function actualizar ($id){
				$data["grupoesEditar"]=$this->grupoe->obtenerporId($id);
				$this->load->view("header");
				$this->load->view("grupoes/actualizar",$data);
				$this->load->view("footer");
			}

			public function procesarActualizacion(){
			$id_ge_ja=$this->input->post("id_ge_ja");
			$datosGrupoeEditado=array(
				"numero_ge_ja"=>$this->input->post("numero_ge_ja"),
				"foto_ge_ja"=>$this->input->post("foto_ge_ja"),
				"pais_ge_ja"=>$this->input->post("pais_ge_ja"),
				"favor_ge_ja"=>$this->input->post("favor_ge_ja"),
				"contra_ge_ja"=>$this->input->post("contra_ge_ja"),
				"diferencia_ge_ja"=>$this->input->post("diferencia_ge_ja"),
				"puntos_ge_ja"=>$this->input->post("puntos_ge_ja")
			);
			if($this->grupoe->actualizar($id_ge_ja,$datosGrupoeEditado)){
			}else{

			}
			redirect('grupoes/index');

		}
}
?>
