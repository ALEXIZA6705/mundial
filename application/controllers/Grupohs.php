<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Grupohs extends CI_Controller {
	public function __construct(){
		parent::__construct();

		$this->load->model("grupoh");
	}
// renderiza la vista estudiantes
	public function index()
	{
		$data["listadoGrupohs"]=$this->grupoh->obtenerTodos();
		$this->load->view('header');
		$this->load->view('grupohs/index',$data);
		$this->load->view('footer');
	}
// renderiza la vista de nuevo
  	public function nuevo()
  	{

  		$this->load->view('header');
  		$this->load->view('grupohs/nuevo');
  		$this->load->view('footer');
  	}
		// funcion para capturar los datos del formulario
		public function guardarGrupoh(){
        $datosNuevoGrupoh=array(
          "numero_gh_ja"=>$this->input->post("numero_gh_ja"),
          "foto_gh_ja"=>$this->input->post("foto_gh_ja"),
          "pais_gh_ja"=>$this->input->post("pais_gh_ja"),
					"favor_gh_ja"=>$this->input->post("favor_gh_ja"),
          "contra_gh_ja"=>$this->input->post("contra_gh_ja"),
					"diferencia_gh_ja"=>$this->input->post("diferencia_gh_ja"),
					"puntos_gh_ja"=>$this->input->post("puntos_gh_ja")

        );

				// INICIO DE PROCESO DE SUBIDA DE FOTOGRAFIA
				$this->load->library("upload");// activando la liibreria de subida de archivos
		    $new_name = "foto_" . time() . "_" . rand(1, 5000);//generando un nombre aleatorio
		    $config['file_name'] = $new_name;
		    $config['upload_path'] = FCPATH . 'uploads/grupohs/';// ruta de subida
		    $config['allowed_types'] = 'jpg|png';//pdf|docx
		    $config['max_size']  = 5*1024;//5MB
		    $this->upload->initialize($config);// inicializar la configuracion
				// validando la subida del aarchivo
		    if ($this->upload->do_upload("foto_gh_ja")) {
		      $dataSubida = $this->upload->data();
		      $datosNuevoGrupoh["foto_gh_ja"] = $dataSubida['file_name'];
		    }
				// FIN DEL PROCESO DE ARCHIVOS


				print_r($datosNuevoGrupoh);
				if ($this->grupoh->insertar($datosNuevoGrupoh)) {
									 				$this->session->set_flashdata('confirmacion','insertado exitosamente');
									 				}else	{
									 			$this->session->set_flashdata('error','verifique e intente de nuevo');
									 				}
									 					redirect('grupohs/index');
									       }


	public function borrar($id_gh_ja){

					if($this->grupoh->eliminarPorId($id_gh_ja)){
						$this->session->set_flashdata('confirmacion','estudiante eliminado exitosamente');
	            }else{
	            $this->session->set_flashdata('error','verifique e intente nuevamnete ');
	            }
							redirect('grupohs/index');
	        }

			//funcion para renderizar eliminar estudiantes
			// ACTUALIZAR
			public function actualizar ($id){
				$data["grupohsEditar"]=$this->grupoh->obtenerporId($id);
				$this->load->view("header");
				$this->load->view("grupohs/actualizar",$data);
				$this->load->view("footer");
			}

			public function procesarActualizacion(){
			$id_gh_ja=$this->input->post("id_gh_ja");
			$datosGrupohEditado=array(
				"numero_gh_ja"=>$this->input->post("numero_gh_ja"),
				"foto_gh_ja"=>$this->input->post("foto_gh_ja"),
				"pais_gh_ja"=>$this->input->post("pais_gh_ja"),
				"favor_gh_ja"=>$this->input->post("favor_gh_ja"),
				"contra_gh_ja"=>$this->input->post("contra_gh_ja"),
				"diferencia_gh_ja"=>$this->input->post("diferencia_gh_ja"),
				"puntos_gh_ja"=>$this->input->post("puntos_gh_ja")
			);
			if($this->grupoh->actualizar($id_gh_ja,$datosGrupohEditado)){
			}else{

			}
			redirect('grupohs/index');

		}
}
?>
