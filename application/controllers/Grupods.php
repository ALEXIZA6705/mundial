<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Grupods extends CI_Controller {
	public function __construct(){
		parent::__construct();

		$this->load->model("grupod");
	}
// renderiza la vista estudiantes
	public function index()
	{
		$data["listadoGrupods"]=$this->grupod->obtenerTodos();
		$this->load->view('header');
		$this->load->view('grupods/index',$data);
		$this->load->view('footer');
	}
// renderiza la vista de nuevo
  	public function nuevo()
  	{

  		$this->load->view('header');
  		$this->load->view('grupods/nuevo');
  		$this->load->view('footer');
  	}
		// funcion para capturar los datos del formulario
		public function guardarGrupod(){
        $datosNuevoGrupod=array(
          "numero_gd_ja"=>$this->input->post("numero_gd_ja"),
          "foto_gd_ja"=>$this->input->post("foto_gd_ja"),
          "pais_gd_ja"=>$this->input->post("pais_gd_ja"),
					"favor_gd_ja"=>$this->input->post("favor_gd_ja"),
          "contra_gd_ja"=>$this->input->post("contra_gd_ja"),
					"diferencia_gd_ja"=>$this->input->post("diferencia_gd_ja"),
					"puntos_gd_ja"=>$this->input->post("puntos_gd_ja")

        );

				// INICIO DE PROCESO DE SUBIDA DE FOTOGRAFIA
				$this->load->library("upload");// activando la liibreria de subida de archivos
		    $new_name = "foto_" . time() . "_" . rand(1, 5000);//generando un nombre aleatorio
		    $config['file_name'] = $new_name;
		    $config['upload_path'] = FCPATH . 'uploads/grupods/';// ruta de subida
		    $config['allowed_types'] = 'jpg|png';//pdf|docx
		    $config['max_size']  = 5*1024;//5MB
		    $this->upload->initialize($config);// inicializar la configuracion
				// validando la subida del aarchivo
		    if ($this->upload->do_upload("foto_gd_ja")) {
		      $dataSubida = $this->upload->data();
		      $datosNuevoGrupod["foto_gd_ja"] = $dataSubida['file_name'];
		      $datosNuevoGrupod["foto_gd_ja"] = $dataSubida['file_name'];
		    }
				// FIN DEL PROCESO DE ARCHIVOS


				print_r($datosNuevoGrupod);
				if ($this->grupod->insertar($datosNuevoGrupod)) {
									 				$this->session->set_flashdata('confirmacion','insertado exitosamente');
									 				}else	{
									 			$this->session->set_flashdata('error','verifique e intente de nuevo');
									 				}
									 					redirect('grupods/index');
									       }


	public function borrar($id_gd_ja){

					if($this->grupod->eliminarPorId($id_gd_ja)){
						$this->session->set_flashdata('confirmacion','estudiante eliminado exitosamente');
	            }else{
	            $this->session->set_flashdata('error','verifique e intente nuevamnete ');
	            }
							redirect('grupods/index');
	        }

			//funcion para renderizar eliminar estudiantes
			// ACTUALIZAR
			public function actualizar ($id){
				$data["grupodsEditar"]=$this->grupod->obtenerporId($id);
				$this->load->view("header");
				$this->load->view("grupods/actualizar",$data);
				$this->load->view("footer");
			}

			public function procesarActualizacion(){
			$id_gd_ja=$this->input->post("id_gd_ja");
			$datosGrupodEditado=array(
				"numero_gd_ja"=>$this->input->post("numero_gd_ja"),
				"foto_gd_ja"=>$this->input->post("foto_gd_ja"),
				"pais_gd_ja"=>$this->input->post("pais_gd_ja"),
				"favor_gd_ja"=>$this->input->post("favor_gd_ja"),
				"contra_gd_ja"=>$this->input->post("contra_gd_ja"),
				"diferencia_gd_ja"=>$this->input->post("diferencia_gd_ja"),
				"puntos_gd_ja"=>$this->input->post("puntos_gd_ja")
			);
			if($this->grupod->actualizar($id_gd_ja,$datosGrupodEditado)){
			}else{

			}
			redirect('grupods/index');

		}
}
?>
