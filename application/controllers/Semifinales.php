<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Semifinales extends CI_Controller {
	public function __construct(){
		parent::__construct();


		$this->load->model("semifinal");
	}
// renderiza la vista estudiantes
	public function index()
	{

		$data["listadoSemifinales"]=$this->semifinal->obtenerTodos();
		$this->load->view('header');

		$this->load->view('semifinales/index',$data);
		$this->load->view('footer');
	}
// renderiza la vista de nuevo
  	public function nuevo()
  	{

  		$this->load->view('header');

  		$this->load->view('semifinales/nuevo');
  		$this->load->view('footer');
  	}
		// funcion para capturar los datos del formulario
		public function guardarSemifinal(){
        $datosNuevoSemifinal=array(


          "foto_sem_ja"=>$this->input->post("foto_sem_ja"),
          "pais_sem_ja"=>$this->input->post("pais_sem_ja"),
					"grupo_sem_ja"=>$this->input->post("grupo_sem_ja"),
        	"goles_sem_ja"=>$this->input->post("goles_sem_ja")

        );

				// INICIO DE PROCESO DE SUBIDA DE FOTOGRAFIA
				$this->load->library("upload");// activando la liibreria de subida de archivos
		    $new_name = "foto_" . time() . "_" . rand(1, 5000);//generando un nombre aleatorio
		    $config['file_name'] = $new_name;

		    $config['upload_path'] = FCPATH . 'uploads/semifinales/';// ruta de subida
		    $config['allowed_types'] = 'jpg|png';//pdf|docx
		    $config['max_size']  = 5*1024;//5MB
		    $this->upload->initialize($config);// inicializar la configuracion
				// validando la subida del aarchivo
		    if ($this->upload->do_upload("foto_sem_ja")) {
		      $dataSubida = $this->upload->data();
		      $datosNuevoSemifinal["foto_sem_ja"] = $dataSubida['file_name'];
		    }
				// FIN DEL PROCESO DE ARCHIVOS


				print_r($datosNuevoSemifinal);
				if ($this->semifinal->insertar($datosNuevoSemifinal)) {
									 				$this->session->set_flashdata('confirmacion','insertado exitosamente');
									 				}else	{
									 			$this->session->set_flashdata('error','verifique e intente de nuevo');
									 				}
									 					redirect('semifinales/index');
									       }


	public function borrar($id_sem_ja){

					if($this->octavo->eliminarPorId($id_sem_ja)){
						$this->session->set_flashdata('confirmacion','estudiante eliminado exitosamente');
	            }else{
	            $this->session->set_flashdata('error','verifique e intente nuevamnete ');
	            }
							redirect('semifinales/index');
	        }

			//funcion para renderizar eliminar estudiantes
			// ACTUALIZAR
			public function actualizar ($id){
				$data["semifinalesEditar"]=$this->semifinal->obtenerporId($id);
				$this->load->view("header");
				$this->load->view("semifinales/actualizar",$data);
				$this->load->view("footer");
			}

			public function procesarActualizacion(){
			$id_sem_ja=$this->input->post("id_sem_ja");
			$datosSemifinalEditado=array(
				"foto_sem_ja"=>$this->input->post("foto_sem_ja"),
				"pais_sem_ja"=>$this->input->post("pais_sem_ja"),
				"grupo_sem_ja"=>$this->input->post("grupo_sem_ja"),
				"goles_sem_ja"=>$this->input->post("goles_sem_ja")
			);
			if($this->semifinal->actualizar($id_sem_ja,$datosSemifinalEditado)){
			}else{

			}
			redirect('semifinales/index');

		}
}
?>
