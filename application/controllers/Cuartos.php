<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cuartos extends CI_Controller {
		public function __construct(){
		parent::__construct();

		$this->load->model("cuarto");

	}
// renderiza la vista estudiantes
	public function index()
	{
		$data["listadoCuartos"]=$this->cuarto->obtenerTodos();
		$this->load->view('header');
		$this->load->view('cuartos/index',$data);
		$this->load->view('footer');
	}
// renderiza la vista de nuevo
  	public function nuevo()
  	{

  		$this->load->view('header');
  		$this->load->view('cuartos/nuevo');
  		$this->load->view('footer');
  	}
		// funcion para capturar los datos del formulario
		public function guardarCuartos(){
        $datosNuevoCuarto=array(

          "foto_cu_ja"=>$this->input->post("foto_cu_ja"),
          "pais_cu_ja"=>$this->input->post("pais_cu_ja"),
					"grupo_cu_ja"=>$this->input->post("grupo_cu_ja"),
        	"goles_cu_ja"=>$this->input->post("goles_cu_ja")

        );

				// INICIO DE PROCESO DE SUBIDA DE FOTOGRAFIA
				$this->load->library("upload");// activando la liibreria de subida de archivos
		    $new_name = "foto_" . time() . "_" . rand(1, 5000);//generando un nombre aleatorio
		    $config['file_name'] = $new_name;
		    $config['upload_path'] = FCPATH . 'uploads/cuartos/';// ruta de subida
		    $config['allowed_types'] = 'jpg|png';//pdf|docx
		    $config['max_size']  = 5*1024;//5MB
		    $this->upload->initialize($config);// inicializar la configuracion
				// validando la subida del aarchivo
		    if ($this->upload->do_upload("foto_cu_ja")) {
		      $dataSubida = $this->upload->data();
		      $datosNuevoCuarto["foto_oc_ja"] = $dataSubida['file_name'];
		    }
				// FIN DEL PROCESO DE ARCHIVOS


				print_r($datosNuevoOctavo);
				if ($this->cuarto->insertar($datosNuevoCuarto)) {
									 				$this->session->set_flashdata('confirmacion','insertado exitosamente');
									 				}else	{
									 			$this->session->set_flashdata('error','verifique e intente de nuevo');
									 				}
									 					redirect('cuartos/index');
									       }


	public function borrar($id_cu_ja){

					if($this->cuarto->eliminarPorId($id_cu_ja)){
						$this->session->set_flashdata('confirmacion',' eliminado exitosamente');
	            }else{
	            $this->session->set_flashdata('error','verifique e intente nuevamnete ');
	            }
							redirect('cuartos/index');
	        }

			//funcion para renderizar eliminar estudiantes
			// ACTUALIZAR
			public function actualizar ($id){
				$data["cuartosEditar"]=$this->cuarto->obtenerporId($id);
				$this->load->view("header");
				$this->load->view("cuartos/actualizar",$data);
				$this->load->view("footer");
			}

			public function procesarActualizacion(){
			$id_cu_ja=$this->input->post("id_cu_ja");
			$datosCuartoEditado=array(
				"foto_cu_ja"=>$this->input->post("foto_cu_ja"),
				"pais_cu_ja"=>$this->input->post("pais_cu_ja"),
				"grupo_cu_ja"=>$this->input->post("grupo_cu_ja"),
				"goles_cu_ja"=>$this->input->post("goles_cu_ja")
			);
			if($this->cuarto->actualizar($id_cu_ja,$datosCuartoEditado)){
			}else{

			}
			redirect('cuartos/index');

		}
}
?>
