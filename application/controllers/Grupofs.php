<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Grupofs extends CI_Controller {
	public function __construct(){
		parent::__construct();

		$this->load->model("grupof");
	}
// renderiza la vista estudiantes
	public function index()
	{
		$data["listadoGrupofs"]=$this->grupof->obtenerTodos();
		$this->load->view('header');
		$this->load->view('grupofs/index',$data);
		$this->load->view('footer');
	}
// renderiza la vista de nuevo
  	public function nuevo()
  	{

  		$this->load->view('header');
  		$this->load->view('grupofs/nuevo');
  		$this->load->view('footer');
  	}
		// funcion para capturar los datos del formulario
		public function guardarGrupof(){
        $datosNuevoGrupof=array(
          "numero_gf_ja"=>$this->input->post("numero_gf_ja"),
          "foto_gf_ja"=>$this->input->post("foto_gf_ja"),
          "pais_gf_ja"=>$this->input->post("pais_gf_ja"),
					"favor_gf_ja"=>$this->input->post("favor_gf_ja"),
          "contra_gf_ja"=>$this->input->post("contra_gf_ja"),
					"diferencia_gf_ja"=>$this->input->post("diferencia_gf_ja"),
					"puntos_gf_ja"=>$this->input->post("puntos_gf_ja")

        );

				// INICIO DE PROCESO DE SUBIDA DE FOTOGRAFIA
				$this->load->library("upload");// activando la liibreria de subida de archivos
		    $new_name = "foto_" . time() . "_" . rand(1, 5000);//generando un nombre aleatorio
		    $config['file_name'] = $new_name;
		    $config['upload_path'] = FCPATH . 'uploads/grupofs/';// ruta de subida
		    $config['allowed_types'] = 'jpg|png';//pdf|docx
		    $config['max_size']  = 5*1024;//5MB
		    $this->upload->initialize($config);// inicializar la configuracion
				// validando la subida del aarchivo
		    if ($this->upload->do_upload("foto_gf_ja")) {
		      $dataSubida = $this->upload->data();
		      $datosNuevoGrupof["foto_gf_ja"] = $dataSubida['file_name'];
		    }
				// FIN DEL PROCESO DE ARCHIVOS


				print_r($datosNuevoGrupof);
				if ($this->grupof->insertar($datosNuevoGrupof)) {
									 				$this->session->set_flashdata('confirmacion','insertado exitosamente');
									 				}else	{
									 			$this->session->set_flashdata('error','verifique e intente de nuevo');
									 				}
									 					redirect('grupofs/index');
									       }


	public function borrar($id_gf_ja){

					if($this->grupof->eliminarPorId($id_gf_ja)){
						$this->session->set_flashdata('confirmacion','estudiante eliminado exitosamente');
	            }else{
	            $this->session->set_flashdata('error','verifique e intente nuevamnete ');
	            }
							redirect('grupofs/index');
	        }

			//funcion para renderizar eliminar estudiantes
			// ACTUALIZAR
			public function actualizar ($id){
				$data["grupofsEditar"]=$this->grupof->obtenerporId($id);
				$this->load->view("header");
				$this->load->view("grupofs/actualizar",$data);
				$this->load->view("footer");
			}

			public function procesarActualizacion(){
			$id_gf_ja=$this->input->post("id_gf_ja");
			$datosGrupofEditado=array(
				"numero_gf_ja"=>$this->input->post("numero_gf_ja"),
				"foto_gf_ja"=>$this->input->post("foto_gf_ja"),
				"pais_gf_ja"=>$this->input->post("pais_gf_ja"),
				"favor_gf_ja"=>$this->input->post("favor_gf_ja"),
				"contra_gf_ja"=>$this->input->post("contra_gf_ja"),
				"diferencia_gf_ja"=>$this->input->post("diferencia_gf_ja"),
				"puntos_gf_ja"=>$this->input->post("puntos_gf_ja")
			);
			if($this->grupof->actualizar($id_gf_ja,$datosGrupofEditado)){
			}else{

			}
			redirect('grupofs/index');

		}
}
?>
