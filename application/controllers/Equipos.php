<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Equipos extends CI_Controller {
	public function __construct(){
		parent::__construct();

		$this->load->model("equipo");
	}
// renderiza la vista estudiantes
	public function index()
	{
		$data["listadoEquipos"]=$this->equipo->obtenerTodos();
		$this->load->view('header');
		$this->load->view('equipos/index',$data);
		$this->load->view('footer');
	}
// renderiza la vista de nuevo
  	public function nuevo()
  	{

  		$this->load->view('header');
  		$this->load->view('equipos/nuevo');
  		$this->load->view('footer');
  	}
		// funcion para capturar los datos del formulario
		public function guardarEquipo(){
        $datosNuevoEquipo=array(
          "continente_ja"=>$this->input->post("continente_ja"),
          "pais_ja"=>$this->input->post("pais_ja"),
          "ranking_ja"=>$this->input->post("ranking_ja"),
          "foto_ja"=>$this->input->post("foto_ja")

        );

				// INICIO DE PROCESO DE SUBIDA DE FOTOGRAFIA
				$this->load->library("upload");// activando la liibreria de subida de archivos
		    $new_name = "foto_" . time() . "_" . rand(1, 5000);//generando un nombre aleatorio
		    $config['file_name'] = $new_name;
		    $config['upload_path'] = FCPATH . 'uploads/equipos/';// ruta de subida
		    $config['allowed_types'] = 'jpg|png';//pdf|docx
		    $config['max_size']  = 5*1024;//5MB
		    $this->upload->initialize($config);// inicializar la configuracion
				// validando la subida del aarchivo
		    if ($this->upload->do_upload("foto_ja")) {
		      $dataSubida = $this->upload->data();
		      $datosNuevoEquipo["foto_ja"] = $dataSubida['file_name'];
		    }
				// FIN DEL PROCESO DE ARCHIVOS


				print_r($datosNuevoEquipo);
				if ($this->equipo->insertar($datosNuevoEquipo)) {
				$this->session->set_flashdata('confirmacion','equipo insertado exitosamente');
				}else	{
			$this->session->set_flashdata('error','verifique e intente de nuevo');
				}
					redirect('equipos/index');
      }
			public function borrar($id_ja){

					if($this->equipo->eliminarPorId($id_ja)){
							$this->session->set_flashdata('confirmacion','equipo eliminado exitosamente');
					}else{
							$this->session->set_flashdata('error','verifique e intente de nuevo');
					}
					redirect('equipos/index');


			}


			//funcion para renderizar eliminar estudiantes
			// ACTUALIZAR
			public function actualizar ($id){
				$data["equiposEditar"]=$this->equipo->obtenerporId($id);
				$this->load->view("header");
				$this->load->view("equipos/actualizar",$data);
				$this->load->view("footer");
			}

			public function procesarActualizacion(){
			$id_eq=$this->input->post("id_ja");
			$datosEquipoEditado=array(
        "continente_ja"=>$this->input->post("continente_ja"),
        "pais_ja"=>$this->input->post("pais_ja"),
        "ranking_ja"=>$this->input->post("ranking_ja"),
        "foto_ja"=>$this->input->post("foto_ja")
			);
			if($this->equipo->actualizar($id_ja,$datosEquipoEditado)){
			}else{

			}
			redirect('equipos/index');

		}
}
