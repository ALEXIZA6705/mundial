<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Grupogs extends CI_Controller {
	public function __construct(){
		parent::__construct();

		$this->load->model("grupog");
	}
// renderiza la vista estudiantes
	public function index()
	{
		$data["listadoGrupogs"]=$this->grupog->obtenerTodos();
		$this->load->view('header');
		$this->load->view('grupogs/index',$data);
		$this->load->view('footer');
	}
// renderiza la vista de nuevo
  	public function nuevo()
  	{

  		$this->load->view('header');
  		$this->load->view('grupogs/nuevo');
  		$this->load->view('footer');
  	}
		// funcion para capturar los datos del formulario
		public function guardarGrupog(){
        $datosNuevoGrupog=array(
          "numero_gg_ja"=>$this->input->post("numero_gg_ja"),
          "foto_gg_ja"=>$this->input->post("foto_gg_ja"),
          "pais_gg_ja"=>$this->input->post("pais_gg_ja"),
					"favor_gg_ja"=>$this->input->post("favor_gg_ja"),
          "contra_gg_ja"=>$this->input->post("contra_gg_ja"),
					"diferencia_gg_ja"=>$this->input->post("diferencia_gg_ja"),
					"puntos_gg_ja"=>$this->input->post("puntos_gg_ja")

        );

				// INICIO DE PROCESO DE SUBIDA DE FOTOGRAFIA
				$this->load->library("upload");// activando la liibreria de subida de archivos
		    $new_name = "foto_" . time() . "_" . rand(1, 5000);//generando un nombre aleatorio
		    $config['file_name'] = $new_name;
		    $config['upload_path'] = FCPATH . 'uploads/grupogs/';// ruta de subida
		    $config['allowed_types'] = 'jpg|png';//pdf|docx
		    $config['max_size']  = 5*1024;//5MB
		    $this->upload->initialize($config);// inicializar la configuracion
				// validando la subida del aarchivo
		    if ($this->upload->do_upload("foto_gg_ja")) {
		      $dataSubida = $this->upload->data();
		      $datosNuevoGrupog["foto_gggja"] = $dataSubida['file_name'];
		    }
				// FIN DEL PROCESO DE ARCHIVOS


				print_r($datosNuevoGrupog);
				if ($this->grupog->insertar($datosNuevoGrupog)) {
									 				$this->session->set_flashdata('confirmacion','insertado exitosamente');
									 				}else	{
									 			$this->session->set_flashdata('error','verifique e intente de nuevo');
									 				}
									 					redirect('grupogs/index');
									       }


	public function borrar($id_gg_ja){

					if($this->grupog->eliminarPorId($id_gg_ja)){
						$this->session->set_flashdata('confirmacion','estudiante eliminado exitosamente');
	            }else{
	            $this->session->set_flashdata('error','verifique e intente nuevamnete ');
	            }
							redirect('grupogs/index');
	        }

			//funcion para renderizar eliminar estudiantes
			// ACTUALIZAR
			public function actualizar ($id){
				$data["grupogsEditar"]=$this->grupog->obtenerporId($id);
				$this->load->view("header");
				$this->load->view("grupogs/actualizar",$data);
				$this->load->view("footer");
			}

			public function procesarActualizacion(){
			$id_gg_ja=$this->input->post("id_gg_ja");
			$datosGrupogEditado=array(
				"numero_gg_ja"=>$this->input->post("numero_gg_ja"),
				"foto_gg_ja"=>$this->input->post("foto_gg_ja"),
				"pais_gg_ja"=>$this->input->post("pais_gg_ja"),
				"favor_gg_ja"=>$this->input->post("favor_gg_ja"),
				"contra_gg_ja"=>$this->input->post("contra_gg_ja"),
				"diferencia_gg_ja"=>$this->input->post("diferencia_gg_ja"),
				"puntos_gg_ja"=>$this->input->post("puntos_gg_ja")
			);
			if($this->grupog->actualizar($id_gg_ja,$datosGrupogEditado)){
			}else{

			}
			redirect('grupogs/index');

		}
}
?>
